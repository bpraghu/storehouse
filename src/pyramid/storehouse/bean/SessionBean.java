package pyramid.storehouse.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pyramid.storehouse.service.MainService;

@ManagedBean
public class SessionBean {

	@ManagedProperty("#{mainService}")
	private MainService mainService;
	
	private static Logger logger = LoggerFactory.getLogger(SessionBean.class);

	public void continueSession () {
		System.out.println ("Dummy ping call to continue the session .. ");
		logger.info ("Dummy ping call to continue the session .. ");
	}

	public MainService getMainService() {
		return mainService;
	}

	public void setMainService(MainService mainService) {
		this.mainService = mainService;
	}
}
