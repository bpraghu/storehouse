package pyramid.storehouse.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pyramid.storehouse.dao.EmployeeDao;
import pyramid.storehouse.dao.MenusDao;
import pyramid.storehouse.dao.PermissionDao;
import pyramid.storehouse.pojo.Menu;
import pyramid.storehouse.pojo.Permission;
import pyramid.storehouse.view.UserView;

public class MenuPermissionCommand {

	@Autowired
	private MenusDao menuDao;

	@Autowired
	private PermissionDao permissionDao;

	@Autowired
	private EmployeeDao employeeDao;

	public PermissionDao getPermissionDao() {
		return permissionDao;
	}

	public void setPermissionDao(PermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}

	public MenusDao getMenuDao() {
		return menuDao;
	}

	public void setMenuDao(MenusDao menuDao) {
		this.menuDao = menuDao;
	}
	
	public EmployeeDao getEmployeeDao() {
		return employeeDao;
	}

	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	public List<Menu> process (UserView user) {

		pyramid.storehouse.view.EmployeeView emp = getEmployeeDao().getEmployee(user.getEmpId());
		int roleId = emp.getRoleId();
		
		List<Menu> menus = getMenuDao().getAllMenus();
		List<Permission> permissions = getPermissionDao ().getAllPermission (roleId);

		for (int i = 0; i < menus.size(); i++) {
			Menu menu = menus.get(i);
			if (permissions.indexOf(new Permission (roleId, menu.getMenuId())) == -1) {
				menus.remove(i);
				// move the counter back once
				--i;
			}
			else if (menus.get(i).getSubMenus() != null) {
				for (int j = 0; j < menu.getSubMenus().size(); j++) {

					if (permissions.indexOf(new Permission (roleId, menu.getSubMenus().get(j).getMenuId())) == -1) {
						menu.getSubMenus().remove(j);
						// move the counter back once
						--j;
					}
				}
			}
		}

		return menus;
	}
}
