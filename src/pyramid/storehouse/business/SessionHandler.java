package pyramid.storehouse.business;

import javax.faces.bean.ManagedBean;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;

import pyramid.storehouse.dao.SessionmapDao;
import pyramid.storehouse.pojo.Sessionmap;
import pyramid.storehouse.view.UserView;

@ManagedBean
public class SessionHandler {

	@Autowired
	ServletContext servletContext;

	@Autowired
	private SessionmapDao sessionmapDao;

	public String createSession (UserView user) {

		Sessionmap sessionMap = sessionmapDao.createSession(user);
		return sessionMap.getClientKey();
	}

	public void deactivateSession (String clientKey) {
		sessionmapDao.deactivateSession(clientKey);
	}

	public void updateLastAccessed (String clientKey) {
		sessionmapDao.updateLastAccessedSession(clientKey);
	}
}
