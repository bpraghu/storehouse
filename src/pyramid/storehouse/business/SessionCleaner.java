package pyramid.storehouse.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pyramid.storehouse.dao.SessionmapDao;

public class SessionCleaner implements Runnable {

	private Logger logger = LoggerFactory.getLogger(SessionCleaner.class.getName());
	
	@Autowired
	private SessionmapDao sessionDao;

	@Override
	public void run() {

		try {
			if (sessionDao != null) {
				sessionDao.cleanAbandonedSessions();
			}
		}
		catch (Exception e) {
			logger.error("An exception occured during the session cleaning process ", e);
		}
	}
}
