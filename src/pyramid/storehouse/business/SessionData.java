package pyramid.storehouse.business;

import java.util.HashMap;
import java.util.Map;

public class SessionData {

	private Map<String, Object> sessionData;
	
	public SessionData () {
		this.sessionData = new HashMap<String, Object> ();
	}

	public SessionData(Map<String, Object> sessionData) {
		this.sessionData = sessionData;
	}

	public void add (String key, Object data) {
		sessionData.put(key, data);
	}

	public Object get (String key) {
		return sessionData.get(key);
	}

	public boolean contains (String key) {
		return sessionData.containsKey(key);
	}

	public boolean containsValue (String key) {
		return sessionData.containsValue (key);
	}
	
	public void remove (String key) {
		sessionData.remove(key);
	}
}
