package pyramid.storehouse;

import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;
 
public class AppListener extends ContextLoaderListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {

		super.contextInitialized(event);

		System.out.println ("======================= Loading Application Context ==============================");
		ServletContext sc = event.getServletContext();
		sc.setAttribute(AppConstants.APPLICATION_DATA, new HashMap());
	}
}
