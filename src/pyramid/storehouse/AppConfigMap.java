package pyramid.storehouse;

import java.util.ArrayList;
import java.util.HashMap;

public class AppConfigMap {

	HashMap<String, ArrayList<String>> data;

	public AppConfigMap () {
		
		data = new HashMap<String, ArrayList<String>>();
	}

	public void put (String key, String value) {

		if (!data.containsKey(key)) {
			ArrayList<String> list = new ArrayList<String> ();
			list.add(value);
			data.put(key, list);
		}
		else {
			ArrayList<String> list = data.get(key);
			list.add(value);
		}
	}

	public ArrayList<String> getConfig (String key) {

		return data.get(key);
	}
}
