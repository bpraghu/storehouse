package pyramid.storehouse.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pyramid.storehouse.dao.ItemDao;
import pyramid.storehouse.dao.StoreDao;
import pyramid.storehouse.pojo.ReportQuery;
import pyramid.storehouse.pojo.ReportQueryFormat;
import pyramid.storehouse.view.StoreView;
import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.view.ItemView;

@Controller
@RequestMapping(value="/action/reports")
public class ReportsController {

	@Autowired
	private StoreDao storeDao;

	@Autowired
	private ItemDao itemDao;

	@Autowired
	private DataSource datasource;

	@RequestMapping(value="/showItemWiseForm")
	public ModelAndView showItemWiseForm () {

		List<ItemView> items = itemDao.getAllItems();
		List<StoreView> stores = storeDao.getAllStores();
		ReportQuery query = new ReportQuery();

		ModelAndView mv = new ModelAndView("itemsReport");
		mv.addObject(items);
		mv.addObject(stores);
		mv.addObject("reportQuery", query);
		return mv;
	}

	@RequestMapping(value="/showItemWiseReport")
	public ModelAndView processItemWiseReport (HttpServletRequest request, ReportQuery rptQuery) {

		String view = "itemsWiseReport";
		
		String reportTitle = "";
		ItemView ItemView = itemDao.getItem (rptQuery.getItemID());
		
		String incomingItemsQuery = "SELECT I.NAME AS 'ITEM_NAME',S.QUANTITY,SUP.NAME AS 'SUPPLIER_NAME', SUP.MOBILE AS 'SUPPLIER_MOBILE',"
				+ " S.DRIVER_MOBILE, S.VEHICLE_REG_NUM, S.REMARKS, S.ENTRY_DATE from ItemView I INNER JOIN Stock S ON I.ITEM_ID=S.ITEM_ID" 
		+ " INNER JOIN STORE STR ON S.STORE_ID = STR.STORE_ID LEFT JOIN SUPPLIER SUP ON S.SUPPLIER_ID=SUP.SUPPLIER_ID"
		+ " WHERE S.STORE_ID= " + rptQuery.getStoreID()
		+ " AND S.ITEM_ID = " + rptQuery.getItemID()
		+ " AND S.OPERATION = 'I'";
		
		String dispersedItemsQuery = "SELECT I.NAME AS 'ITEM_NAME', S.QUANTITY, S.REMARKS, S.ENTRY_DATE from ItemView I INNER JOIN Stock S ON I.ITEM_ID=S.ITEM_ID" 
		+ " INNER JOIN STORE STR ON S.STORE_ID = STR.STORE_ID LEFT JOIN SUPPLIER SUP ON S.SUPPLIER_ID=SUP.SUPPLIER_ID"
		+ " WHERE S.STORE_ID= " + rptQuery.getStoreID()
		+ " AND S.ITEM_ID = " + rptQuery.getItemID()
		+ " AND S.OPERATION = 'O'";

		if (rptQuery.getMonth() != 0) {
			incomingItemsQuery += " AND S.ENTRY_DATE BETWEEN " + DateUtil.getMonthQuery (rptQuery.getMonth() - 1);
			dispersedItemsQuery += " AND S.ENTRY_DATE BETWEEN " + DateUtil.getMonthQuery (rptQuery.getMonth() - 1);
			reportTitle = ItemView.getName() + " stock updates for the month of " + DateUtil.getMonthName(rptQuery.getMonth());
		}
		else {
			incomingItemsQuery += " AND S.ENTRY_DATE BETWEEN '" + DateUtil.format(rptQuery.getFromDate()) + "' AND '" + DateUtil.format(rptQuery.getEndDate()) + "'";
			dispersedItemsQuery += " AND S.ENTRY_DATE BETWEEN '" + DateUtil.format(rptQuery.getFromDate()) + "' AND '" + DateUtil.format(rptQuery.getEndDate()) + "'";
			reportTitle = ItemView.getName() + " stock updates between " + DateUtil.formatIST (rptQuery.getFromDate()) + " and " + DateUtil.formatIST (rptQuery.getEndDate());
		}

		incomingItemsQuery += " ORDER BY I.NAME, S.OPERATION";
		dispersedItemsQuery += " ORDER BY I.NAME, S.OPERATION";

		ModelAndView mv = new ModelAndView(view);
		mv.addObject("incomingItemsQuery", incomingItemsQuery);
		mv.addObject("dispersedItemsQuery", dispersedItemsQuery);
		mv.addObject("Datasource", datasource);
		mv.addObject("ITEM_NAME", ItemView.getName());
		mv.addObject("reportTitle", reportTitle);
		return mv;
	}

	@RequestMapping(value="/showSiteWiseReport")
	public ModelAndView processSiteWiseReport (HttpServletRequest request, ReportQuery rptQuery) {

		String view = "storeWiseReport";

		String reportTitle = "";
		List<ItemView> items = itemDao.getAllItems ();
		List<ReportQueryFormat> reportQueryList = new ArrayList<ReportQueryFormat>();

		for (ItemView ItemView : items) {

			String incomingItemsQuery = "SELECT I.NAME AS 'ITEM_NAME',S.QUANTITY,SUP.NAME AS 'SUPPLIER_NAME', SUP.MOBILE AS 'SUPPLIER_MOBILE',"
					+ " S.DRIVER_MOBILE, S.VEHICLE_REG_NUM, S.REMARKS, S.ENTRY_DATE from ItemView I INNER JOIN Stock S ON I.ITEM_ID=S.ITEM_ID" 
			+ " INNER JOIN STORE STR ON S.STORE_ID = STR.STORE_ID LEFT JOIN SUPPLIER SUP ON S.SUPPLIER_ID=SUP.SUPPLIER_ID"
			+ " WHERE S.STORE_ID= " + rptQuery.getStoreID()
			+ " AND S.ITEM_ID = " + ItemView.getItemId()
			+ " AND S.OPERATION = 'I'";
			
			String dispersedItemsQuery = "SELECT I.NAME AS 'ITEM_NAME', S.QUANTITY, S.REMARKS, S.ENTRY_DATE from ItemView I INNER JOIN Stock S ON I.ITEM_ID=S.ITEM_ID" 
			+ " INNER JOIN STORE STR ON S.STORE_ID = STR.STORE_ID LEFT JOIN SUPPLIER SUP ON S.SUPPLIER_ID=SUP.SUPPLIER_ID"
			+ " WHERE S.STORE_ID= " + rptQuery.getStoreID()
			+ " AND S.ITEM_ID = " + ItemView.getItemId()
			+ " AND S.OPERATION = 'O'";
	
			if (rptQuery.getMonth() != 0) {
				incomingItemsQuery += " AND S.ENTRY_DATE BETWEEN " + DateUtil.getMonthQuery (rptQuery.getMonth() - 1);
				dispersedItemsQuery += " AND S.ENTRY_DATE BETWEEN " + DateUtil.getMonthQuery (rptQuery.getMonth() - 1);
				reportTitle = ItemView.getName() + " stock updates for the month of " + DateUtil.getMonthName(rptQuery.getMonth());
			}
			else {
				incomingItemsQuery += " AND S.ENTRY_DATE BETWEEN '" + DateUtil.format(rptQuery.getFromDate()) + "' AND '" + DateUtil.format(rptQuery.getEndDate()) + "'";
				dispersedItemsQuery += " AND S.ENTRY_DATE BETWEEN '" + DateUtil.format(rptQuery.getFromDate()) + "' AND '" + DateUtil.format(rptQuery.getEndDate()) + "'";
				reportTitle = ItemView.getName() + " stock updates between " + DateUtil.formatIST (rptQuery.getFromDate()) + " and " + DateUtil.formatIST (rptQuery.getEndDate());
			}
	
			incomingItemsQuery += " ORDER BY I.NAME, S.OPERATION";
			dispersedItemsQuery += " ORDER BY I.NAME, S.OPERATION";

			reportQueryList.add(new ReportQueryFormat(reportTitle, incomingItemsQuery, dispersedItemsQuery));
		}

		ModelAndView mv = new ModelAndView(view);
		mv.addObject("Datasource", datasource);
		mv.addObject("ReportQueries", reportQueryList);
		return mv;
	}
}
