package pyramid.storehouse.vo;


import java.util.Date;

public class SessionmapVO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7488001679897815491L;
	private String clientKey;
	private String userName;
	private String masterKey;
	private Date lastAccessed;

	public SessionmapVO() {
	}

	public SessionmapVO(String clientKey, String userName, String masterKey) {
		this.clientKey = clientKey;
		this.userName = userName;
		this.masterKey = masterKey;
	}

	public SessionmapVO(String clientKey, String userName, String masterKey,
			Date lastAccessed) {
		this.clientKey = clientKey;
		this.userName = userName;
		this.masterKey = masterKey;
		this.lastAccessed = lastAccessed;
	}

	public String getClientKey() {
		return this.clientKey;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMasterKey() {
		return this.masterKey;
	}

	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}

	public Date getLastAccessed() {
		return this.lastAccessed;
	}

	public void setLastAccessed(Date lastAccessed) {
		this.lastAccessed = lastAccessed;
	}

	@Override
	public String toString() {
		return "SessionmapVO [clientKey=" + clientKey + ", userName="
				+ userName + ", masterKey=" + masterKey + ", lastAccessed="
				+ lastAccessed + "]";
	}
}
