package pyramid.storehouse.vo;

public class BaseForm {

	private String generalError;

	public String getGeneralError() {
		return generalError;
	}

	public void setGeneralError(String generalError) {
		this.generalError = generalError;
	}
}
