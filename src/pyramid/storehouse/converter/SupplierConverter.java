package pyramid.storehouse.converter;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pyramid.storehouse.service.SupplierService;
import pyramid.storehouse.view.SupplierView;
 
 
@FacesConverter("supplierConverter")
public class SupplierConverter implements Converter {
 
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
    	
    	System.out.println("*************** VALUE " + value);
    	
        if(value != null && value.trim().length() > 0) {
        	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        	SupplierService service = (SupplierService)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "supplierService");
            return service.getSuppliers().get(Integer.parseInt(value) - 1).getSupplierId();
        	/*List<SupplierView> supps = service.getSuppliers();
        	for (SupplierView supplier : supps) {
        		if (supplier.getSupplierName().equalsIgnoreCase(value)){
        			return supplier;
        		}
        	}*/
        }
        return null;
    }
 
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
    	
    	/*System.out.println ("========== " + object);
        if(object != null) {
        	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        	SupplierService service = (SupplierService)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "supplierService");
        	List<SupplierView> supps = service.getSuppliers();
        	for (SupplierView supplier : supps) {
        		if(supplier.getSupplierId() == ((Integer)object)) {
        			return supplier.getSupplierName();
        		}
        	}
            return String.valueOf(object);
        }
        else {
            return null;
        }*/
    	
    	
    	System.out.println ("========== " + object);
        if(object != null) {
        	/*ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        	SupplierService service = (SupplierService)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "supplierService");
        	List<SupplierView> supps = service.getSuppliers();
        	for (SupplierView supplier : supps) {
        		if(supplier.getSupplierId() == ((Integer)object)) {
        			return supplier.getSupplierName();
        		}
        	}*/
        	
        	SupplierView supplier = (SupplierView)object;
            return String.valueOf(supplier.getSupplierId());
        }
        else {
            return null;
        }
    }   
}