package pyramid.storehouse.converter;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pyramid.storehouse.service.ItemService;
import pyramid.storehouse.view.ItemView;
 
 
@FacesConverter("itemConverter")
public class ItemConverter implements Converter {
 
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
    	
    	System.out.println("*************** VALUE " + value);
    	
        /*if(value != null && value.trim().length() > 0) {
        	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        	//ItemService service = (ItemService)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("itemService");
        	ItemService service = (ItemService)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "itemService");
            return service.getItems().get(Integer.parseInt(value) - 1).getItemId();
        	List<SupplierView> supps = service.getSuppliers();
        	for (SupplierView supplier : supps) {
        		if (supplier.getSupplierName().equalsIgnoreCase(value)){
        			return supplier;
        		}
        	}
        }*/
        return null;
    }
 
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
    	
    	/*System.out.println ("========== " + object);
        if(object != null) {
        	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        	SupplierService service = (SupplierService)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "supplierService");
        	List<SupplierView> supps = service.getSuppliers();
        	for (SupplierView supplier : supps) {
        		if(supplier.getSupplierId() == ((Integer)object)) {
        			return supplier.getSupplierName();
        		}
        	}
            return String.valueOf(object);
        }
        else {
            return null;
        }*/
    	
    	
    	System.out.println ("========== " + object);
        if(object != null) {
        	/*ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        	SupplierService service = (SupplierService)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "supplierService");
        	List<SupplierView> supps = service.getSuppliers();
        	for (SupplierView supplier : supps) {
        		if(supplier.getSupplierId() == ((Integer)object)) {
        			return supplier.getSupplierName();
        		}
        	}*/
        	
        	ItemView item = (ItemView)object;
            return String.valueOf(item.getItemId());
        }
        else {
            return null;
        }
    }   
}