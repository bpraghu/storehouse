package pyramid.storehouse.pojo;

import java.util.Date;

public class Sessionmap implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6146653653061100187L;
	private String clientKey;
	private String userName;
	private String masterKey;
	private Date lastAccessed;
	private Date loggedIn;
	private Date loggedOut;
	private int active;

	public Sessionmap() {
	}

	public Sessionmap(String clientKey, String userName, String masterKey) {
		this.clientKey = clientKey;
		this.userName = userName;
		this.masterKey = masterKey;
	}

	public Sessionmap(String clientKey, String userName, String masterKey,
			Date lastAccessed) {
		this.clientKey = clientKey;
		this.userName = userName;
		this.masterKey = masterKey;
		this.lastAccessed = lastAccessed;
	}

	public String getClientKey() {
		return this.clientKey;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMasterKey() {
		return this.masterKey;
	}

	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}

	public Date getLastAccessed() {
		return this.lastAccessed;
	}

	public void setLastAccessed(Date lastAccessed) {
		this.lastAccessed = lastAccessed;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Date getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Date loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Date getLoggedOut() {
		return loggedOut;
	}

	public void setLoggedOut(Date loggedOut) {
		this.loggedOut = loggedOut;
	}
}
