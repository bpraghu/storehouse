package pyramid.storehouse.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import pyramid.storehouse.vo.BaseForm;

public class Supplier extends BaseForm implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2762709443758814799L;
	private int supplierId;
	private String supplierName;
	private String address;
	private String city;
	private String state;
	private String country;

	private String pincode;

	private String mobile;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dateAdded;
	private Date dateModded;

	public Supplier() {
	}
	
	public Supplier(int supplierId, String supplierName, String address,
			String city, String state, String country, String pincode,
			Date startDate, Date closeDate, String mobile) {
		super();
		this.supplierId = supplierId;
		this.supplierName = supplierName;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
		this.dateAdded = startDate;
		this.dateModded = closeDate;
		this.mobile = mobile;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Date getDateModded() {
		return dateModded;
	}

	public void setDateModded(Date dateModded) {
		this.dateModded = dateModded;
	}

	public void reset() {
		this.supplierName = null;
		this.address = null;
		this.city = null;
		this.pincode = null;
		this.dateAdded = null;
		this.dateModded = null;
		this.mobile = null;
	}
}
