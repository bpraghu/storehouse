package pyramid.storehouse.pojo;

public class AppConfig {

	private int appConfigId;
	private String appConfigName;
	private String appConfigValue;
	
	public AppConfig(int appConfigId, String appConfigName,
			String appConfigValue) {
		super();
		this.appConfigId = appConfigId;
		this.appConfigName = appConfigName;
		this.appConfigValue = appConfigValue;
	}

	public int getAppConfigId() {
		return appConfigId;
	}
	public void setAppConfigId(int appConfigId) {
		this.appConfigId = appConfigId;
	}
	public String getAppConfigName() {
		return appConfigName;
	}
	public void setAppConfigName(String appConfigName) {
		this.appConfigName = appConfigName;
	}
	public String getAppConfigValue() {
		return appConfigValue;
	}
	public void setAppConfigValue(String appConfigValue) {
		this.appConfigValue = appConfigValue;
	}

	@Override
	public String toString() {
		return "AppConfig [appConfigId=" + appConfigId + ", appConfigName="
				+ appConfigName + ", appConfigValue=" + appConfigValue + "]";
	}
}
