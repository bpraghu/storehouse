package pyramid.storehouse.pojo;

import java.util.ArrayList;
import java.util.List;

public class Menu {

	private boolean permitted;
	private int menuId;
	private String menuName;
	private int parentMenuId;
	private String url;
	private List<Menu> subMenus;

	public Menu () {
	}

	public Menu (int menuId) {
		this.menuId = menuId;
	}
	
	public Menu(int menuId, String menuName, int parentMenuId, String url) {
		super();
		this.menuId = menuId;
		this.menuName = menuName;
		this.parentMenuId = parentMenuId;
		this.url = url;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public int getParentMenuId() {
		return parentMenuId;
	}
	public void setParentMenuId(int parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public List<Menu> getSubMenus() {
		return subMenus;
	}
	public void setSubMenus(List<Menu> subMenus) {
		this.subMenus = subMenus;
	}
	
	public void addSubMenus(Menu menu) {
		if(this.subMenus == null)
			this.subMenus = new ArrayList <Menu> ();

		this.subMenus.add(menu);
	}
	
	public void deleteSubmenu (Menu menu) {
		if(this.subMenus == null)
			return;

		this.subMenus.remove(menu);
	}
	
	@Override
	public boolean equals(Object menu) {

		return this.menuId == ((Menu)menu).getMenuId();
	}
	@Override
	public String toString() {
		return "Menu [menuId=" + menuId + ", menuName=" + menuName
				+ ", parentMenuId=" + parentMenuId + ", url=" + url + "]";
	}

	public boolean isPermitted() {
		return permitted;
	}

	public void setPermitted(boolean permitted) {
		this.permitted = permitted;
	}
}
