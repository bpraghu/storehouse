package pyramid.storehouse.pojo;


import java.util.Date;


public class Item implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4356588800622239863L;
	private int itemId;
	private String name;
	private String unit;
	private Date dateAdded;

	public Item() {
	}

	public Item(int itemId, Date dateAdded) {
		this.itemId = itemId;
		this.dateAdded = dateAdded;
	}

	public Item(int itemId, String name, Date dateAdded) {
		this.itemId = itemId;
		this.name = name;
		this.dateAdded = dateAdded;
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public Date getDateAdded() {
		return this.dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void reset () {
		this.itemId = 0;
		this.name = null;
		this.unit = null;
		this.dateAdded = null;
	}

	public String save () {
		
		return "";
	}
	
	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", name=" + name + ", unit=" + unit
				+ ", dateAdded=" + dateAdded + "]";
	}
}
