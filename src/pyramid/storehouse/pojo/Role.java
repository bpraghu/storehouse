package pyramid.storehouse.pojo;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import pyramid.storehouse.dao.RoleDao;

@ManagedBean
public class Role implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -162238534183783160L;
	private Integer roleId;
	private String roleName;
	private String description;
	private String read;
	private String write;
	private String modify;
	private String delete;
	private Date dateAdded;
	private String selectedRole;
	private List<Role> roles;
	
	@ManagedProperty("#{roleDao}")
	private RoleDao roleDao;

	public Role() {
	}
	
	public Role(int roleId, String roleName, String description) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.description = description;
	}

	public Role(String roleName, String description, String read, String write,
			String modify, String delete) {
		this.roleName = roleName;
		this.description = description;
		this.read = read;
		this.write = write;
		this.modify = modify;
		this.delete = delete;
	}

	public Role(String roleName, String description, String read, String write,
			String modify, String delete, Date dateAdded) {
		this.roleName = roleName;
		this.description = description;
		this.read = read;
		this.write = write;
		this.modify = modify;
		this.delete = delete;
		this.dateAdded = dateAdded;
	}

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRead() {
		return this.read;
	}

	public void setRead(String read) {
		this.read = read;
	}

	public String getWrite() {
		return this.write;
	}

	public void setWrite(String write) {
		this.write = write;
	}

	public String getModify() {
		return this.modify;
	}

	public void setModify(String modify) {
		this.modify = modify;
	}

	public String getDelete() {
		return this.delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public Date getDateAdded() {
		return this.dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public void showRoleSelect() {
		System.out.println ("Fired");
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 640);
        options.put("height", 340);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");

        HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put("ck", Arrays.asList(request.getParameter("ck")));
        RequestContext.getCurrentInstance().openDialog("role.xhtml", options, params);
    //    return "role.xhtml";
    }
	
	public void searchRole () {
		System.out.println("Searching for Roles");
	}

	@PostConstruct
	public void showRoles () {
		roles = roleDao.getAllRoles();
	}
	
	@Override
	public String toString() {
		return "Role [roleId=" + roleId + ", roleName=" + roleName
				+ ", description=" + description + ", read=" + read
				+ ", write=" + write + ", modify=" + modify + ", delete="
				+ delete + ", dateAdded=" + dateAdded + "]";
	}

	public String getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(String selectedRole) {
		this.selectedRole = selectedRole;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
}
