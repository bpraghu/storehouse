package pyramid.storehouse.pojo;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ReportQuery implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3727389140882102406L;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fromDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;
	private int month;
	private int storeID;
	private int itemID;

	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getStoreID() {
		return storeID;
	}
	public void setStoreID(int storeID) {
		this.storeID = storeID;
	}
	public int getItemID() {
		return itemID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	@Override
	public String toString() {
		return "ReportQuery [fromDate=" + fromDate + ", endDate=" + endDate
				+ ", month=" + month + ", storeID=" + storeID + ", itemID="
				+ itemID + "]";
	}
}
