package pyramid.storehouse.pojo;

import java.util.Date;

public class Stock implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6659622279611936526L;
	private String stockId;
	private String supplierId;
	private String supplierName;
	private String itemId;
	// Used only for Showing Stocks
	private String itemName;
	private String employeeId;
	private String storeId;
	private String operation;
	
	private String quantity;

	private String driverMobile;

	private String vehicleRegNum;
	private Date entryDate;
	private Date modifyDate;
	private String remarks;

	public Stock() {
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Date getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getDriverMobile() {
		return driverMobile;
	}

	public void setDriverMobile(String driverMobile) {
		this.driverMobile = driverMobile;
	}

	public String getVehicleRegNum() {
		return vehicleRegNum;
	}

	public void setVehicleRegNum(String vehicleRegNum) {
		this.vehicleRegNum = vehicleRegNum;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public void reset () {

		supplierId = null;
		supplierName = null;
		itemId = null;
		employeeId = null;
		storeId = null;
		operation = null;
		quantity = null;
		driverMobile = null;
		vehicleRegNum = null;
		entryDate = null;
		modifyDate = null;
		remarks = null;
	}

	@Override
	public String toString() {
		return "Stock [stockId=" + stockId + ", supplierId=" + supplierId
				+ ", supplierName=" + supplierName + ", itemId=" + itemId
				+ ", employeeId=" + employeeId + ", storeId=" + storeId
				+ ", operation=" + operation + ", quantity=" + quantity
				+ ", driverMobile=" + driverMobile + ", vehicleRegNum="
				+ vehicleRegNum + ", entryDate=" + entryDate + ", modifyDate="
				+ modifyDate + ", remarks=" + remarks + "]";
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
}
