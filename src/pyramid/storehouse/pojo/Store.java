package pyramid.storehouse.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Store implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4201333085399843066L;

	private int storeId;

	private String storeName;

	private String address;
	private String city;
	private String state;
	private String country;

	private String pincode;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date startDate;
	private Date closeDate;

	// Used for Date
	private String date_1_1;
	// Used for Month
	private String date_1_2;
	// Used for Year
	private String date_1_3;
	
	private String empid;

	public Store() {
	}

	public Store(int storeId, String storeName, String address, Date startDate) {
		this.storeId = storeId;
		this.storeName = storeName;
		this.address = address;
		this.startDate = startDate;
	}

	public Store(int storeId, String storeName, String address, String city,
			String pincode, Date startDate, Date closeDate) {
		this.storeId = storeId;
		this.storeName = storeName;
		this.address = address;
		this.city = city;
		this.pincode = pincode;
		this.startDate = startDate;
		this.closeDate = closeDate;
	}

	public int getStoreId() {
		return this.storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return this.storeName;
	}

	public String getEmpid() {
		return empid;
	}

	public void setEmpid(String empid) {
		this.empid = empid;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getCloseDate() {
		return this.closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDate_1_1() {
		return date_1_1;
	}

	public void setDate_1_1(String date_1_1) {
		this.date_1_1 = date_1_1;
	}

	public String getDate_1_2() {
		return date_1_2;
	}

	public void setDate_1_2(String date_1_2) {
		this.date_1_2 = date_1_2;
	}

	public String getDate_1_3() {
		return date_1_3;
	}

	public void setDate_1_3(String date_1_3) {
		this.date_1_3 = date_1_3;
	}

	
	public void reset() {
		this.storeName = null;
		this.address = null;
		this.city = null;
		this.pincode = null;
		this.startDate = null;
		this.closeDate = null;
		this.date_1_1 = null;
		this.date_1_2 = null;
		this.date_1_3 = null;
	}
	
	@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", storeName=" + storeName
				+ ", address1=" + address + ", city=" + city + ", state=" + state + ", country="
				+ country + ", pincode=" + pincode + ", startDate=" + startDate
				+ ", closeDate=" + closeDate + "]";
	}
}
