package pyramid.storehouse.pojo;

public class ReportQueryFormat {

	private String title;
	private String incomingStockQuery;
	private String dispersedStockQuery;
	
	public ReportQueryFormat(String title, String incomingStockQuery,
			String dispersedStockQuery) {
		super();
		this.title = title;
		this.incomingStockQuery = incomingStockQuery;
		this.dispersedStockQuery = dispersedStockQuery;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIncomingStockQuery() {
		return incomingStockQuery;
	}
	public void setIncomingStockQuery(String incomingStockQuery) {
		this.incomingStockQuery = incomingStockQuery;
	}
	public String getDispersedStockQuery() {
		return dispersedStockQuery;
	}
	public void setDispersedStockQuery(String dispersedStockQuery) {
		this.dispersedStockQuery = dispersedStockQuery;
	}
}
