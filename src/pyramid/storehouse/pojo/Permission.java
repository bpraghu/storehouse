package pyramid.storehouse.pojo;

public class Permission {

	private int id;
	private int roleId;
	private int menuId;

	public Permission () {
	}
	
	public Permission(int id, int roleId, int menuId) {
		super();
		this.id = id;
		this.roleId = roleId;
		this.menuId = menuId;
	}

	public Permission(int roleId, int menuId) {
		super();
		this.roleId = roleId;
		this.menuId = menuId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permission other = (Permission) obj;
		if (menuId != other.menuId)
			return false;
		if (roleId != other.roleId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Permission [id=" + id + ", roleId=" + roleId + ", menuId="
				+ menuId + "]";
	}
}
