package pyramid.storehouse;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pyramid.storehouse.business.SessionHandler;

public class StoreHouseAppListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent args) {

		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(args.getSession().getServletContext());
		SessionHandler sessionHandler = (SessionHandler)context.getBean("sessionHandler");
		sessionHandler.deactivateSession(args.getSession().getId());
	}	
}
