
package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.view.EmployeeView;

public interface EmployeeDao {

	public EmployeeView getEmployee (int empId);

	public List<EmployeeView> getAllEmployees ();

	public List<EmployeeView> getAllEmployees (String colName, String criteria);
	
	public List<EmployeeView> getAllEmployees (String colName, String criteria, boolean withoutLoginIDS);

	public void saveEmployee (EmployeeView emp);

	public EmployeeView findEmployeeByMobile (EmployeeView emp);

	public List<EmployeeView> getUnlinkedEmployees ();
}
