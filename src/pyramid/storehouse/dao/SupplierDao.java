package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.view.SupplierView;

public interface SupplierDao {

	public List<SupplierView> getAllSupplier();

	public boolean saveSupplier (SupplierView supplier);

	public List<SupplierView> getAllSupplier (String colName, String criteria);

	public SupplierView findSupplierByMobile(SupplierView supplier);

	public SupplierView getSupplier (String supplierID);
	
	public boolean updateSupplier(SupplierView supplier);
}
