package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.view.ItemView;

public class ItemDaoImpl extends BaseDao implements ItemDao {

	private static Logger logger = LoggerFactory.getLogger(ItemDaoImpl.class);
	
	@Override
	public List<ItemView> getAllItems() {

		String SQL_ALL_ITEMS = "select * from Item";

		List<ItemView> items = this.getNamedParameterJdbcTemplate().query (SQL_ALL_ITEMS, new RowMapper<ItemView> () {

			@Override
			public ItemView mapRow(ResultSet rs, int rowNum) throws SQLException {

				ItemView ItemView = new ItemView ();
				ItemView.setItemId (rs.getInt("ITEM_ID"));
				ItemView.setName(rs.getString("NAME"));
				ItemView.setUnit(rs.getString("UNIT"));
				ItemView.setDateAdded(rs.getDate("DATE_ADDED"));
				return ItemView;
			}
		});
		return items;
	}

	@Override
	public boolean addItem(ItemView ItemView) {

		String SQL_ITEM_INSERT = "INSERT INTO Item (`NAME`,`UNIT`,`DATE_ADDED`) VALUES (:NAME,:UNIT,:DATE_ADDED)";

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("NAME", ItemView.getName());
		paramMaps.put("UNIT", ItemView.getUnit());
		paramMaps.put("DATE_ADDED",  DateUtil.getCurrentDate());
		
		boolean storeAddStatus = false;
		try {
			this.getNamedParameterJdbcTemplate().update(SQL_ITEM_INSERT, paramMaps);
			storeAddStatus = true;
		}
		catch (Exception e) {
			logger.error("An error occured while trying to add an ItemView, parameters " + paramMaps.toString());
			storeAddStatus = false;
		}
		return storeAddStatus;
	}

	@Override
	public boolean updateItem(ItemView ItemView) {

		String SQL_ITEM_UPDATE = "UPDATE Item SET `NAME`=:NAME, `UNIT`=:UNIT, `DATE_ADDED`=:DATE_ADDED where ITEM_ID=:ITEM_ID";

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("ITEM_ID", ItemView.getItemId());
		paramMaps.put("NAME", ItemView.getName());
		paramMaps.put("UNIT", ItemView.getUnit());
		paramMaps.put("DATE_ADDED",  DateUtil.getCurrentDate());

		boolean storeAddStatus = false;
		try {
			this.getNamedParameterJdbcTemplate().update(SQL_ITEM_UPDATE, paramMaps);
			storeAddStatus = true;
		}
		catch (Exception e) {
			logger.error("An error occured while trying to updating an ItemView, parameters " + paramMaps.toString());
			storeAddStatus = false;
		}
		return storeAddStatus;
	}

	@Override
	public ItemView getItem (int itemID) {

		String SQL_ALL_ITEMS = "SELECT * FROM Item WHERE ITEM_ID=:ITEM_ID";

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("ITEM_ID", itemID);

		ItemView ItemView = this.getNamedParameterJdbcTemplate().queryForObject(SQL_ALL_ITEMS, paramMaps, new RowMapper<ItemView> () {

			@Override
			public ItemView mapRow(ResultSet rs, int rowNum) throws SQLException {

				ItemView ItemView = new ItemView ();
				ItemView.setItemId (rs.getInt("ITEM_ID"));
				ItemView.setName(rs.getString("NAME"));
				ItemView.setUnit(rs.getString("UNIT"));
				ItemView.setDateAdded(rs.getDate("DATE_ADDED"));
				return ItemView;
			}
		});
		return ItemView;
	}
	
	@Override
	public ItemView getItem (String itemName) {

		String SQL_ALL_ITEMS = "SELECT * FROM Item WHERE NAME=:ITEM_NAME";

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("ITEM_NAME", itemName);

		ItemView ItemView = this.getNamedParameterJdbcTemplate().queryForObject(SQL_ALL_ITEMS, paramMaps, new RowMapper<ItemView> () {

			@Override
			public ItemView mapRow(ResultSet rs, int rowNum) throws SQLException {

				ItemView ItemView = new ItemView ();
				ItemView.setItemId (rs.getInt("ITEM_ID"));
				ItemView.setName(rs.getString("NAME"));
				ItemView.setUnit(rs.getString("UNIT"));
				ItemView.setDateAdded(rs.getDate("DATE_ADDED"));
				return ItemView;
			}
		});
		return ItemView;
	}
}
