package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.util.DBUtil;
import pyramid.storehouse.view.StockView;

public class StockDaoImpl extends BaseDao implements StockDao {

	private static Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);
	
	@Override
	public void stockEntry(StockView stock) {

/*		String SQL_STOCK_ENTRY = "INSERT INTO `pyramid_schema`.`stock` ( `STORE_ID`,`ITEM_ID`,`EMP_ID`,`OPERATION`,`QUANTITY`,`SUPPLIER_ID`,`DRIVER_MOBILE`,`VEHICLE_REG_NUM`,"
				+ "`REMARKS`,`ENTRY_DATE`) VALUES (:STORE_ID,:ITEM_ID,:EMP_ID,:OPERATION,:QUANTITY,:SUPPLIER_ID,:DRIVER_MOBILE,:VEHICLE_REG_NUM,:REMARKS,:ENTRY_DATE)";
		*/
		String SQL_STOCK_ENTRY = "{CALL INSERT_STOCK (:STORE_ID,:ITEM_ID,:EMP_ID,:OPERATION,:QUANTITY,:VOLUME,:SUPPLIER_ID,:DRIVER_MOBILE,:VEHICLE_REG_NUM,:REMARKS,:ENTRY_DATE,:TO_STORE_ID,:FROM_STORE_ID)}";
		

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("STORE_ID", stock.getStoreId());
		paramMaps.put("ITEM_ID", stock.getItemId());
		paramMaps.put("EMP_ID", stock.getEmployeeId());
		paramMaps.put("OPERATION", stock.getOperation());
		paramMaps.put("QUANTITY", stock.getQuantity());
		paramMaps.put("VOLUME", stock.getVolume());
		paramMaps.put("SUPPLIER_ID", stock.getSupplierId());
		paramMaps.put("DRIVER_MOBILE", stock.getDriverMobile());
		paramMaps.put("VEHICLE_REG_NUM", stock.getVehicleRegNum());
		paramMaps.put("REMARKS", stock.getRemarks());
		paramMaps.put("ENTRY_DATE", stock.getEntryDate());
		paramMaps.put("TO_STORE_ID", stock.getToStoreId());
		paramMaps.put("FROM_STORE_ID", stock.getFromStoreId());

		this.getNamedParameterJdbcTemplate().update(SQL_STOCK_ENTRY, paramMaps);
	}

	@Override
	public List<StockView> getAllStockUpdates (int storeID) {

		HashMap<String, Object> params = new HashMap <String, Object>();
		params.put("STORE_ID", storeID);
		List<StockView> allStocks = this.getNamedParameterJdbcTemplate().query("{CALL GET_STOCK_UPDATE (:STORE_ID)}", params, new RowMapper <StockView> () {

			@Override
			public StockView mapRow(ResultSet rs, int rowCountr)
					throws SQLException {

				StockView stock = new StockView ();
				stock.setItemId(rs.getInt("ITEM_ID"));
				stock.setItemName(rs.getString("ITEM_NAME"));
				stock.setQuantity(rs.getLong("STOCK"));
				stock.setVolume(rs.getLong("BALANCE_VOLUME"));
				return stock;
			}
		});
		return allStocks;
	}

	@Override
	public StockView getStockUpdateByStoreAndStockID (int storeID, int itemID) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STORE_ID", storeID);
		params.put("ITEM_ID", itemID);
		StockView stock = this.getNamedParameterJdbcTemplate().query("{CALL GET_STOCK_BAL_UPDATE_BY_STORE_AND_ITEM_ID (:STORE_ID, :ITEM_ID) }", params, new ResultSetExtractor<StockView>() {

			@Override
			public StockView extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				StockView stock = null;
				if(rs != null && rs.next()) {
					stock = new StockView ();
					stock.setItemId(rs.getInt("ITEM_ID"));
					stock.setItemName(rs.getString("ITEM_NAME"));
					stock.setUnit(rs.getString("UNIT"));
					stock.setQuantity(rs.getLong("STOCK"));
					stock.setVolume(rs.getLong("BALANCE_VOLUME"));
				}
				return stock;
			}
		});
		return stock;
	}

	@Override
	public void stockBalanceUpdate(StockView stock) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STORE_ID", stock.getStoreId());
		params.put("ITEM_ID", stock.getItemId());
		params.put("QUANTITY", stock.getQuantity());
		params.put("VOLUME", stock.getVolume());

		this.getNamedParameterJdbcTemplate().update("{CALL BALANCE_STOCK_UPDATE (:STORE_ID, :ITEM_ID, :QUANTITY, :VOLUME) }", params);
	}

	@Override
	public List<StockView> getStockTransactions(int storeID, int itemID, Date fromDate, Date tillDate, String operation) {

		HashMap<String, Object> params = new HashMap <String, Object>();
		params.put("STORE_ID", storeID);
		params.put("ITEM_ID", itemID);
		params.put("FROM_DATE", fromDate);
		params.put("END_DATE", tillDate);
		params.put("OPERATION", operation);
		List<StockView> allStocks = this.getNamedParameterJdbcTemplate().query("{CALL GET_STOCK_TRANSACTIONS (:STORE_ID, :ITEM_ID, :FROM_DATE, :END_DATE, :OPERATION)}", params, new RowMapper <StockView> () {

			@Override
			public StockView mapRow(ResultSet rs, int rowCountr)
					throws SQLException {

				StockView stock = new StockView ();
				stock.setStockId(rs.getLong("STOCK_ID"));
				stock.setItemName(rs.getString("ITEM_NAME"));
				stock.setQuantity(Math.abs(rs.getLong("QUANTITY")));
				stock.setVolume(Math.abs(rs.getLong("VOLUME")));
				stock.setSupplierName(rs.getString("SUPPLIER_NAME"));
				stock.setDriverMobile(rs.getString("DRIVER_MOBILE"));
				stock.setVehicleRegNum(rs.getString("VEHICLE_REG_NUM"));
				stock.setRemarks(rs.getString("REMARKS"));
				stock.setToStoreId(rs.getInt("TO_STORE_ID"));
				stock.setFromStoreId(rs.getInt("FROM_STORE_ID"));
				return stock;
			}
		});
		return allStocks;

	}

	@Override
	public StockView getStockTransaction(Long stockID) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STOCK_ID", stockID);
		StockView stock = this.getNamedParameterJdbcTemplate().query("{CALL GET_STOCK_BY_TRANID (:STOCK_ID) }", params, new ResultSetExtractor<StockView>() {

			@Override
			public StockView extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				StockView stock = null;
				if(rs != null && rs.next()) {
					stock = new StockView ();
					stock.setStockId(rs.getLong("STOCK_ID"));
					stock.setItemId(rs.getInt("ITEM_ID"));
					stock.setStoreId(rs.getInt("STORE_ID"));
					stock.setEmployeeId(rs.getString("EMP_ID"));
					stock.setOperation(rs.getString("OPERATION"));
					stock.setQuantity(Math.abs(rs.getLong("QUANTITY")));
					stock.setVolume(Math.abs(rs.getLong("VOLUME")));
					stock.setSupplierId(rs.getLong("SUPPLIER_ID"));
					stock.setDriverMobile(rs.getString("DRIVER_MOBILE"));
					stock.setVehicleRegNum(rs.getString("VEHICLE_REG_NUM"));
					stock.setEntryDate(rs.getDate("ENTRY_DATE"));
					stock.setModifyDate(rs.getDate("MODIFY_DATE"));
					stock.setRemarks(rs.getString("REMARKS"));
					stock.setToStoreId(DBUtil.getInteger(rs.getInt("TO_STORE_ID")));
					stock.setFromStoreId(DBUtil.getInteger(rs.getInt("FROM_STORE_ID")));
				}
				return stock;
			}
		});
		return stock;
	}

	@Override
	public void stockUpdate(StockView stock) {
		String SQL_STOCK_UPDATE = "{CALL UPDATE_STOCK (:STOCK_ID, :STORE_ID,:ITEM_ID,:EMP_ID,:OPERATION,:QUANTITY,:VOLUME,:SUPPLIER_ID,:DRIVER_MOBILE,:VEHICLE_REG_NUM,:REMARKS,:ENTRY_DATE, :MODIFY_DATE, :TO_STORE_ID, :FROM_STORE_ID)}";

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("STOCK_ID", stock.getStockId());
		paramMaps.put("STORE_ID", stock.getStoreId());
		paramMaps.put("ITEM_ID", stock.getItemId());
		paramMaps.put("EMP_ID", stock.getEmployeeId());
		paramMaps.put("OPERATION", stock.getOperation());
		paramMaps.put("QUANTITY", stock.getQuantity());
		paramMaps.put("VOLUME", stock.getVolume());
		paramMaps.put("SUPPLIER_ID", stock.getSupplierId());
		paramMaps.put("DRIVER_MOBILE", stock.getDriverMobile());
		paramMaps.put("VEHICLE_REG_NUM", stock.getVehicleRegNum());
		paramMaps.put("REMARKS", stock.getRemarks());
		paramMaps.put("ENTRY_DATE", stock.getEntryDate());
		paramMaps.put("MODIFY_DATE", stock.getModifyDate());
		paramMaps.put("TO_STORE_ID", stock.getToStoreId());
		paramMaps.put("FROM_STORE_ID", stock.getFromStoreId());

		this.getNamedParameterJdbcTemplate().update(SQL_STOCK_UPDATE, paramMaps);
	}
}
