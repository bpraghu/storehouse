package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.view.SupplierView;

public class SupplierDaoImpl extends BaseDao implements SupplierDao {

	private static Logger logger = LoggerFactory.getLogger(SupplierDaoImpl.class);
	
	@Override
	public List<SupplierView> getAllSupplier() {

		return getAllSupplier(null, null);
	}

	@Override
	public boolean saveSupplier(SupplierView supplier) {

		boolean saveStatus = true;
		String SQL_SUPPLIER = "INSERT INTO supplier (`NAME`,`ADDRESS`,`MOBILE`,`CITY`,`PINCODE`,`STATE`,`COUNTRY`,`DATE_ADDED`) " +
									"VALUES (:NAME,:ADDRESS,:MOBILE,:CITY,:PINCODE,:STATE,:COUNTRY,:DATE_ADDED)";
		
		if (supplier.getSupplierId() > 0) {
			SQL_SUPPLIER = "UPDATE SUPPLIER SET NAME = :NAME , ADDRESS = :ADDRESS, MOBILE = :MOBILE, CITY = :CITY, PINCODE = :PINCODE,"
					+ " STATE = :STATE, COUNTRY = :COUNTRY, DATE_ADDED = :DATE_ADDED WHERE SUPPLIER_ID = :SUPPLIER_ID";
		}

		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("NAME", supplier.getSupplierName());
		paramMap.put("ADDRESS", supplier.getAddress());
		paramMap.put("MOBILE", supplier.getMobile());
		paramMap.put("CITY", supplier.getCity());
		paramMap.put("PINCODE", supplier.getPincode());
		paramMap.put("STATE", supplier.getState());
		paramMap.put("COUNTRY", supplier.getCountry());
		paramMap.put("DATE_ADDED",  DateUtil.getCurrentDate());
		paramMap.put("SUPPLIER_ID", supplier.getSupplierId());

		try {
			this.getNamedParameterJdbcTemplate().update(SQL_SUPPLIER, paramMap);
		}
		catch (Exception e) {
			logger.error("An error occurred while trying to add a supplier, parameters " + paramMap.toString(), e);
			saveStatus = false;
		}
		return saveStatus;
	}
	
	@Override
	public boolean updateSupplier(SupplierView supplier) {

		boolean updateStatus = true;
		String SQL_SUPPLIER_UPDATE = "UPDATE SUPPLIER SET NAME = :NAME , ADDRESS = :ADDRESS, MOBILE = :MOBILE, CITY = :CITY, PINCODE = :PINCODE,"
				+ " STATE = :STATE, COUNTRY = :COUNTRY, DATE_ADDED = :DATE_ADDED, DATE_MODIFY = :DATE_MODIFY WHERE SUPPLIER_ID = :SUPPLIER_ID";

		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("NAME", supplier.getSupplierName());
		paramMap.put("ADDRESS", supplier.getAddress());
		paramMap.put("MOBILE", supplier.getMobile());
		paramMap.put("CITY", supplier.getCity());
		paramMap.put("PINCODE", supplier.getPincode());
		paramMap.put("STATE", supplier.getState());
		paramMap.put("COUNTRY", supplier.getCountry());
		paramMap.put("DATE_ADDED",  DateUtil.getCurrentDate());
		paramMap.put("SUPPLIER_ID", supplier.getSupplierId());

		try {
			this.getNamedParameterJdbcTemplate().update(SQL_SUPPLIER_UPDATE, paramMap);
		}
		catch (Exception e) {
			logger.error("An error occurred while trying to add a supplier, parameters " + paramMap.toString() );
			updateStatus = false;
		}
		return updateStatus;
	}

	@Override
	public List<SupplierView> getAllSupplier(String colName, String criteria) {

		String SUPPLIER_SEARCH_QUERY = getSupplierSearchSQL (colName, criteria);
		
		List<SupplierView> supplierList = this.getNamedParameterJdbcTemplate().query (SUPPLIER_SEARCH_QUERY, new RowMapper<SupplierView> () {

			@Override
			public SupplierView mapRow(ResultSet rs, int rowNum) throws SQLException {

				SupplierView supplier = new SupplierView ();
				supplier.setSupplierId(rs.getInt("SUPPLIER_ID"));
				supplier.setSupplierName(rs.getString("NAME"));
				supplier.setAddress(rs.getString("ADDRESS"));
			    supplier.setMobile(rs.getString("MOBILE"));
			    supplier.setCity(rs.getString("CITY"));
			    supplier.setPincode(rs.getString("PINCODE"));
			    supplier.setState(rs.getString("STATE"));
			    supplier.setCountry(rs.getString("COUNTRY"));
			    supplier.setDateAdded(rs.getDate("DATE_ADDED"));
			    supplier.setDateModded(rs.getDate("DATE_MODIFY"));
				return supplier;
			}
			
		});
		return supplierList;
	}
	
	private String getSupplierSearchSQL (String colName, String criteria) {

		String EMP_SEARCH_QUERY = "select * from supplier";

		if (colName != null && colName.trim().length() > 0) {
			EMP_SEARCH_QUERY += " where " + colName + " like '%" + criteria + "%'";
		}
		return EMP_SEARCH_QUERY;
	}

	@Override
	public SupplierView findSupplierByMobile(SupplierView supplier) {

		List<SupplierView> supplierList = getAllSupplier ("MOBILE", supplier.getMobile());
		if (supplierList != null && supplierList.size() > 0) {
			return supplierList.get(0);
		}
		return null;
	}

	@Override
	public SupplierView getSupplier(String supplierID) {

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("SUPPLIER_ID", supplierID);
		SupplierView supplier = this.getNamedParameterJdbcTemplate().queryForObject("SELECT * FROM SUPPLIER WHERE SUPPLIER_ID=:SUPPLIER_ID", paramMaps, new RowMapper<SupplierView> () {

			@Override
			public SupplierView mapRow(ResultSet rs, int rowNum) throws SQLException {

				SupplierView supplier = new SupplierView ();
				supplier.setSupplierId(rs.getInt("SUPPLIER_ID"));
				supplier.setSupplierName(rs.getString("NAME"));
				supplier.setAddress(rs.getString("ADDRESS"));
			    supplier.setMobile(rs.getString("MOBILE"));
			    supplier.setCity(rs.getString("CITY"));
			    supplier.setPincode(rs.getString("PINCODE"));
			    supplier.setState(rs.getString("STATE"));
			    supplier.setCountry(rs.getString("COUNTRY"));
			    supplier.setDateAdded(rs.getDate("DATE_ADDED"));
			    supplier.setDateModded(rs.getDate("DATE_MODIFY"));
				return supplier;
			}
			
		});
		return supplier;
	}
}
