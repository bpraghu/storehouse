package pyramid.storehouse.dao;

import pyramid.storehouse.pojo.Sessionmap;
import pyramid.storehouse.view.UserView;

public interface SessionmapDao {

	public Sessionmap createSession(UserView user);
	
	public Sessionmap updateLastAccessedSession(String clientKey);

	public Sessionmap getSessionmap (String clientKey);

	public void deactivateSession (String clientKey);

	public void cleanAbandonedSessions ();
}
