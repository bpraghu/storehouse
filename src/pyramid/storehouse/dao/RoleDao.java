package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.pojo.Role;

public interface RoleDao {

	public List<Role> getAllRoles();

	public Role getRole (int roleId);
}
