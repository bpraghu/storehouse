package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.pojo.Menu;

public interface MenusDao {

	public List<Menu> getAllMenus ();

	public List<Menu> getAllMenus (int parentId);

	public List<Menu> getAllMenusForRole (int roleId);
}
