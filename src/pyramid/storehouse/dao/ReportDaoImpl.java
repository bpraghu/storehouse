package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.view.ReportDataView;
import pyramid.storehouse.view.ReportView;
import pyramid.storehouse.view.TransferView;

public class ReportDaoImpl extends BaseDao implements ReportDao {

	private String generateStoreWiseItemReportSQL (int storeID, int itemID, Date fromDt, Date tillDt) {
		
		String SQL_WITH_ITEMID = "SELECT I.NAME AS 'ITEM_NAME', I.ITEM_ID,S.OPERATION, S.QUANTITY, S.VOLUME,SUP.NAME AS 'SUPPLIER_NAME', SUP.MOBILE AS 'SUPPLIER_MOBILE', S.DRIVER_MOBILE,  " + 
				 "S.VEHICLE_REG_NUM, S.REMARKS, S.ENTRY_DATE from Item I INNER JOIN Stock S ON I.ITEM_ID=S.ITEM_ID INNER JOIN STORE STR  " + 
				 "ON S.STORE_ID = STR.STORE_ID LEFT JOIN SUPPLIER SUP ON S.SUPPLIER_ID=SUP.SUPPLIER_ID " + 
				 "WHERE S.STORE_ID= %s and I.ITEM_ID = %s and S.ENTRY_DATE between '%s' and '%s'  ORDER BY S.OPERATION, I.NAME, S.ENTRY_DATE, S.OPERATION;";

		String SQL_WITHOUT_ITEMID = "SELECT I.NAME AS 'ITEM_NAME', I.ITEM_ID,S.OPERATION, S.QUANTITY, S.VOLUME,SUP.NAME AS 'SUPPLIER_NAME', SUP.MOBILE AS 'SUPPLIER_MOBILE', S.DRIVER_MOBILE,  " + 
				 "S.VEHICLE_REG_NUM, S.REMARKS, S.ENTRY_DATE from Item I INNER JOIN Stock S ON I.ITEM_ID=S.ITEM_ID INNER JOIN STORE STR  " + 
				 "ON S.STORE_ID = STR.STORE_ID LEFT JOIN SUPPLIER SUP ON S.SUPPLIER_ID=SUP.SUPPLIER_ID " + 
				 "WHERE S.STORE_ID= %s and S.ENTRY_DATE between '%s' and '%s'  ORDER BY I.NAME, S.ENTRY_DATE, S.OPERATION;";

		String SQL = null;
		
		if (itemID == 0) {
			SQL = String.format(SQL_WITHOUT_ITEMID, storeID, DateUtil.format(fromDt), DateUtil.format(DateUtil.setEODTimeStamp(tillDt)));
		}
		else {
			SQL = String.format(SQL_WITH_ITEMID, storeID, itemID, DateUtil.format(fromDt), DateUtil.format(DateUtil.setEODTimeStamp(tillDt)));
		}
		return SQL;
	}
	
	@Override
	public List<ReportDataView> generateStoreWiseItemReport (ReportView queryData) {

		String SQL = generateStoreWiseItemReportSQL(queryData.getStoreId(), queryData.getItemId(), queryData.getFromDate(), queryData.getEndDate());
		List<ReportDataView> reportList = this.getNamedParameterJdbcTemplate().query (SQL, new RowMapper<ReportDataView> () {

			@Override
			public ReportDataView mapRow(ResultSet rs, int rowNum) throws SQLException {

				ReportDataView rpt = new ReportDataView ();
				rpt.setItemID(rs.getInt("ITEM_ID"));
				rpt.setItemName(rs.getString("ITEM_NAME"));
				rpt.setOperation(rs.getString("OPERATION"));
				rpt.setQuantity(Math.abs(rs.getLong("QUANTITY")));
				rpt.setVolume(Math.abs(rs.getLong("VOLUME")));
				rpt.setSupplierName(rs.getString("SUPPLIER_NAME"));
				rpt.setSupplierMobile(rs.getString("SUPPLIER_MOBILE"));
				rpt.setDriverMobile(rs.getString("DRIVER_MOBILE"));
				rpt.setVehicleRegNum(rs.getString("VEHICLE_REG_NUM"));
				rpt.setRemarks(rs.getString("REMARKS"));
				rpt.setEntryDate(rs.getTimestamp("ENTRY_DATE"));
				return rpt;
			}
			
		});
		return reportList;
	}

	@Override
	public List<ReportDataView> getStoreWiseItemReport (int storeID, Date tillDate) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STORE_ID", storeID);
		params.put("TILL_DT", tillDate);
		List<ReportDataView> allStocks = this.getNamedParameterJdbcTemplate().query("{CALL GET_STOCK_CHECK_ON_STOCK_STORE_DATE (:STORE_ID, :TILL_DT) }", params, new RowMapper <ReportDataView> () {

			@Override
			public ReportDataView mapRow(ResultSet rs, int rowCountr)
					throws SQLException {

				ReportDataView stock = new ReportDataView ();
				stock.setItemID(rs.getInt("ITEM_ID"));
				stock.setItemName(rs.getString("ITEM_NAME"));
				stock.setQuantity(rs.getLong("STOCK"));
				return stock;
			}
		});
		return allStocks;
	}

	@Override
	public List<ReportDataView> getStockUpdateForStoreTillDate(int storeID, Date tillDate) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STORE_ID", storeID);
		params.put("TILL_DT", tillDate);
		List<ReportDataView> allStocks = this.getNamedParameterJdbcTemplate().query("{CALL GET_STOCK_UPDATE_FOR_STORE_BY_DATE (:STORE_ID, :TILL_DT) }", params, new RowMapper <ReportDataView> () {

			@Override
			public ReportDataView mapRow(ResultSet rs, int rowCountr)
					throws SQLException {

				ReportDataView stock = new ReportDataView ();
				stock.setItemID(rs.getInt("ITEM_ID"));
				stock.setItemName(rs.getString("ITEM_NAME"));
				stock.setOperation(rs.getString("OPERATION"));
				stock.setQuantity(Math.abs(rs.getLong("STOCK")));
				stock.setVolume(Math.abs(rs.getLong("VOLUME")));
				return stock;
			}
		});
		return allStocks;
	}

	@Override
	public List<TransferView> getTransferFromReport(int fromStoreID, Date fromDate, Date tillDate) {

		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STORE_ID", fromStoreID);
		params.put("FROM_DATE", fromDate);
		params.put("TILL_DATE", tillDate);
		List<TransferView> allTransfers = this.getNamedParameterJdbcTemplate().query("{CALL GET_TRANSFERS_FROM_STORE (:STORE_ID, :FROM_DATE, :TILL_DATE) }", params, new RowMapper <TransferView> () {

			@Override
			public TransferView mapRow(ResultSet rs, int rowCountr)
					throws SQLException {

				TransferView transferEntry = new TransferView ();
				transferEntry.setStoreName(rs.getString("STORE_NAME"));
				transferEntry.setItemName(rs.getString("ITEM_NAME"));
				transferEntry.setQuantity(Math.abs(rs.getLong("QUANTITY")));
				transferEntry.setVolume(Math.abs(rs.getLong("VOLUME")));
				transferEntry.setRemark(rs.getString("REMARKS"));
				transferEntry.setTransferDate(rs.getDate("TRANSFER_DATE"));
				return transferEntry;
			}
		});
		return allTransfers;
	}
	
	@Override
	public List<TransferView> getTransferToReport(int fromStoreID, Date fromDate, Date tillDate) {

		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("STORE_ID", fromStoreID);
		params.put("FROM_DATE", fromDate);
		params.put("TILL_DATE", tillDate);
		List<TransferView> allTransfers = this.getNamedParameterJdbcTemplate().query("{CALL GET_TRANSFERS_TO_STORE (:STORE_ID, :FROM_DATE, :TILL_DATE) }", params, new RowMapper <TransferView> () {

			@Override
			public TransferView mapRow(ResultSet rs, int rowCountr)
					throws SQLException {

				TransferView transferEntry = new TransferView ();
				transferEntry.setStoreName(rs.getString("STORE_NAME"));
				transferEntry.setItemName(rs.getString("ITEM_NAME"));
				transferEntry.setQuantity(Math.abs(rs.getLong("QUANTITY")));
				transferEntry.setVolume(Math.abs(rs.getLong("VOLUME")));
				transferEntry.setRemark(rs.getString("REMARKS"));
				transferEntry.setTransferDate(rs.getDate("TRANSFER_DATE"));
				return transferEntry;
			}
		});
		return allTransfers;
	}
}
