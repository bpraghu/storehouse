package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.pojo.Role;

public class RoleDaoImpl extends BaseDao implements RoleDao {

	@Override
	public List<Role> getAllRoles() {

		String SQL_SELECT_ROLES = "select * from Role";

		List<Role> roles = getNamedParameterJdbcTemplate().query(
				SQL_SELECT_ROLES, new RowMapper<Role>() {

					@Override
					public Role mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						Role role = new Role();
						role.setRoleId(rs.getInt("ROLE_ID"));
						role.setRoleName(rs.getString("ROLE_NAME"));
						role.setDescription(rs.getString("DESCRIPTION"));
						role.setRead(rs.getString("READ"));
						role.setWrite(rs.getString("WRITE"));
						role.setModify(rs.getString("DELETE"));
						role.setDelete(rs.getString("MODIFY"));
						role.setDateAdded(rs.getDate("DATE_ADDED"));
						return role;
					}

				});
		return roles;
	}

	@Override
	public Role getRole (int roleId) {

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("role_id", roleId);
		Role role = this.getNamedParameterJdbcTemplate().queryForObject("select * from role where role_id=:role_id", paramMaps, new RowMapper<Role> () {

			@Override
			public Role mapRow(ResultSet rs, int rowNum)
					throws SQLException {

				Role role = new Role();
				role.setRoleId(rs.getInt("ROLE_ID"));
				role.setRoleName(rs.getString("ROLE_NAME"));
				role.setDescription(rs.getString("DESCRIPTION"));
				role.setRead(rs.getString("READ"));
				role.setWrite(rs.getString("WRITE"));
				role.setModify(rs.getString("DELETE"));
				role.setDelete(rs.getString("MODIFY"));
				role.setDateAdded(rs.getDate("DATE_ADDED"));
				return role;
			}
		});
		return role;
	}
}
