package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.view.UserView;

public interface UserDao {

	public List loadUsers ();

	public boolean authenticate (UserView user);

	public pyramid.storehouse.view.UserView getUser (String userName);

	public pyramid.storehouse.view.UserView getUser (UserView user);

	public boolean createLogin (UserView user);

	public boolean changePassword (UserView user);
}
