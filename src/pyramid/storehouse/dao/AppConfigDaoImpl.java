package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import pyramid.storehouse.AppConfigMap;

public class AppConfigDaoImpl extends BaseDao implements AppConfigDao {

	@Override
	public AppConfigMap getAllConfigs () {

		String SQL_ALL_CONFIGS = "select * from APPCONFIG ORDER BY APP_CONFIG_NAME";
		AppConfigMap configMap = getNamedParameterJdbcTemplate().query(SQL_ALL_CONFIGS, new ResultSetExtractor <AppConfigMap>() {

			@Override
			public AppConfigMap extractData(ResultSet rs) throws SQLException,
					DataAccessException {

				AppConfigMap configs = new AppConfigMap ();
				while (rs.next()) {

					configs.put(rs.getString("APP_CONFIG_NAME"), rs.getString("APP_CONFIG_VALUE"));
				}
				return configs;
			}
		});
		return configMap;
	}
}
