package pyramid.storehouse.dao;

import pyramid.storehouse.AppConfigMap;

public interface AppConfigDao {

	public AppConfigMap getAllConfigs ();
}
