/**
 * 
 */
package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.view.UserView;


/**
 * @author GOKUL
 * 
 */
public class UserDaoImpl extends BaseDao implements UserDao {

	/*
	 * (non-Javadoc)
	 * 
	 * @see pyramid.storehouse.dao.UserDao#loadUsers()
	 */
	@Override
	public List loadUsers() {

		return null;
	}

	@Override
	public boolean authenticate(UserView user) {

		return getUser (user) != null;
	}

	@Override
	public pyramid.storehouse.view.UserView getUser(String userName) {

		HashMap<String, String> paramMaps = new HashMap<String, String>();
		paramMaps.put("userName", userName);

		UserView user = null;

		user = this.getNamedParameterJdbcTemplate().queryForObject(
				"select * from user where user_name=:userName", paramMaps,
				new RowMapper<UserView>() {

					@Override
					public UserView mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						UserView user = new UserView();
						user.setEmpId(rs.getInt("EMP_ID"));
						user.setUserName(rs.getString("USER_NAME"));
						user.setPassword(rs.getString("PASSWORD"));
						return user;
					}
				});

		return user;
	}

	@Override
	public UserView getUser(UserView user) {
		HashMap<String, String> paramMaps = new HashMap<String, String>();
		paramMaps.put("USERNAME", user.getUserName());

		try {
			user = this
					.getNamedParameterJdbcTemplate()
					.queryForObject(
							"{CALL GET_USER (:USERNAME)}",
							paramMaps, new RowMapper<UserView>() {

								@Override
								public UserView mapRow(ResultSet rs, int rowNum)
										throws SQLException {

									UserView user = new UserView();
									user.setEmpId(rs.getInt("EMP_ID"));
									user.setUserName(rs.getString("USER_NAME"));
									user.setPassword(rs.getString("PASSWORD"));
									String activeStatus = rs.getString("ACTIVE");
									user.setActive ((activeStatus == null || activeStatus.equalsIgnoreCase("A"))?true:false);
									user.setRoleId(rs.getInt("ROLE_ID"));
									user.setStoreId(rs.getInt("STORE_ID"));
									return user;
								}
							});
		} catch (Exception e) {
			logger.error("User with User ID " + user.getUserName() + " not found", e);
			user = null;
		}
		return user;
	}

	/*@Override
	public UserView getUser (String userName) {

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("USER_NAME", userName);
		
		UserView user = null;
		
		try {
			user = this.getNamedParameterJdbcTemplate().queryForObject ("select * from User where USER_NAME = :USER_NAME",params, new RowMapper<UserView> () {
	
				@Override
				public UserView mapRow(ResultSet rs, int rowNum) throws SQLException {
	
					UserView user = new UserView ();
					user.setEmpId(rs.getInt("EMP_ID"));
					user.setUserName(rs.getString("USER_NAME"));
					user.setPassword(rs.getString("PASSWORD"));
					return user;
				}
				
			});
		}
		catch (EmptyResultDataAccessException excep) {
			logger.error ("No User Found");
		}
		return user;
	}*/

	@Override
	public boolean changePassword(UserView user) {

		boolean passwordUpdateStatus = false;
		String SQL = "update user set Password= :PASSWORD where emp_id=:EMP_ID";
		
		Map<String, Object> data = new HashMap<String, Object> ();
		data.put("EMP_ID", user.getEmpId());
		data.put("PASSWORD", user.getPassword());

		try {
			this.getNamedParameterJdbcTemplate().update(SQL, data);
			passwordUpdateStatus = true;
		}
		catch (Exception e) {
			logger.error("An error occurred while trying to update password" + data.toString(), e);
			passwordUpdateStatus = false;
		}
		return passwordUpdateStatus;
	}
	
	@Override
	public boolean createLogin(UserView user) {

		boolean createLoginStatus = true;
		String SQL = "INSERT INTO user (EMP_ID, USER_NAME, PASSWORD) VALUES (:EMP_ID, :USER_NAME, :PASSWORD)";
		Map<String, Object> data = new HashMap<String, Object> ();
		data.put("EMP_ID", user.getEmpId());
		data.put("USER_NAME", user.getUserName());
		data.put("PASSWORD", user.getPassword());

		try {
			this.getNamedParameterJdbcTemplate().update(SQL, data);
			createLoginStatus = true;
		}
		catch (Exception e) {
			logger.error("An error occurred while trying to save Employee, parameters " + data.toString(), e);
			createLoginStatus = false;
		}
		return createLoginStatus;
	}
}
