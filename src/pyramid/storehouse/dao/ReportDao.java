package pyramid.storehouse.dao;

import java.util.Date;
import java.util.List;

import pyramid.storehouse.view.ReportDataView;
import pyramid.storehouse.view.ReportView;
import pyramid.storehouse.view.TransferView;

public interface ReportDao {

	public List<ReportDataView> generateStoreWiseItemReport (ReportView queryData);

	public List<ReportDataView> getStoreWiseItemReport (int storeID, Date tillDate);

	public List<ReportDataView> getStockUpdateForStoreTillDate (int storeID, Date tillDate);

	public List<TransferView> getTransferFromReport (int fromStoreID, Date fromDate, Date tillDate);
	
	public List<TransferView> getTransferToReport (int fromStoreID, Date fromDate, Date tillDate);
}
