package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.view.StoreView;

public class StoreDaoImpl extends BaseDao implements StoreDao {

	private static Logger logger = LoggerFactory.getLogger(StoreDaoImpl.class);  
	@Autowired
	protected EmployeeDao employeeDao;
	
	@Override
	public void saveStore(StoreView store) {

		String STORE_INSERT_SQL = "INSERT INTO store" +
								"(`STORE_NAME`,`ADDRESS`,`CITY`,`PINCODE`,`STATE`,`COUNTRY`,`START_DATE`,`CLOSE_DATE`,`EMP_ID`)" +
								"VALUES (:STORE_NAME ,:ADDRESS ,:CITY ,:PINCODE ,:STATE ,:COUNTRY, :START_DATE , " +
								":CLOSE_DATE ,:EMP_ID)";
		
		if(store.getStoreId() > 0) {

			STORE_INSERT_SQL = "UPDATE store " +
					"set STORE_NAME = :STORE_NAME, ADDRESS = :ADDRESS, CITY = :CITY, PINCODE = :PINCODE, STATE = :STATE, COUNTRY = :COUNTRY, START_DATE = :START_DATE, "
					+ "CLOSE_DATE = :CLOSE_DATE, EMP_ID = :EMP_ID " +
					" WHERE STORE_ID = :STORE_ID";
		}
		

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("STORE_NAME", store.getStoreName());
		paramMaps.put("ADDRESS", store.getAddress());
		paramMaps.put("CITY", store.getCity());
		paramMaps.put("PINCODE", store.getPincode());
		paramMaps.put("STATE", store.getState());
		paramMaps.put("COUNTRY", store.getCountry());
		paramMaps.put("START_DATE", store.getStartDate());
		paramMaps.put("CLOSE_DATE", store.getCloseDate());
		paramMaps.put("EMP_ID", store.getEmpId());
		paramMaps.put("STORE_ID", store.getStoreId());

		this.getNamedParameterJdbcTemplate().update(STORE_INSERT_SQL, paramMaps);
	}

	@Override
	public StoreView getStore(int empId) {

		String SQL_STORE_SELECT = "select * from Store where emp_id=:empId";
		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("empId", empId);

		StoreView store = this.getNamedParameterJdbcTemplate().queryForObject(SQL_STORE_SELECT, paramMaps, new RowMapper <StoreView>() {

			@Override
			public StoreView mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				StoreView store = new StoreView ();
				store.setStoreId(rs.getInt("STORE_ID"));
				store.setStoreName(rs.getString ("STORE_NAME"));
				store.setAddress(rs.getString ("ADDRESS"));
				store.setCity(rs.getString ("CITY"));
				store.setPincode(rs.getString ("PINCODE"));
				store.setState(rs.getString ("STATE"));
				store.setCountry(rs.getString ("COUNTRY"));
				store.setStartDate(rs.getDate ("START_DATE"));
				store.setCloseDate(rs.getDate ("CLOSE_DATE"));
				store.setEmpId(rs.getInt ("EMP_ID"));
				return store;
			}
		});
		return store;
	}

	@Override
	public List<StoreView> getAllStores() {
		String SQL_STORE_SELECT = "{call GET_ALL_STORES }";

		List<StoreView> stores = this.getNamedParameterJdbcTemplate().query(SQL_STORE_SELECT, new RowMapper <StoreView>() {

			@Override
			public StoreView mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				StoreView store = new StoreView ();
				store.setStoreId(rs.getInt("STORE_ID"));
				store.setStoreName(rs.getString ("STORE_NAME"));
				store.setAddress(rs.getString ("ADDRESS"));
				store.setCity(rs.getString ("CITY"));
				store.setPincode(rs.getString ("PINCODE"));
				store.setState(rs.getString ("STATE"));
				store.setCountry(rs.getString ("COUNTRY"));
				store.setStartDate(rs.getDate ("START_DATE"));
				store.setCloseDate(rs.getDate ("CLOSE_DATE"));
				store.setEmpId(rs.getInt ("EMP_ID"));
				store.setEmpName(rs.getString ("FIRST_NAME"));
				return store;
			}
		});
		return stores;
	}

	@Override
	public StoreView getStoreByStoreId(int storeId) {
		String SQL_STORE_SELECT = "select * from Store where store_id=:storeId";
		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("storeId", storeId);

		StoreView store = this.getNamedParameterJdbcTemplate().queryForObject(SQL_STORE_SELECT, paramMaps, new RowMapper <StoreView>() {

			@Override
			public StoreView mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				StoreView store = new StoreView ();
				store.setStoreId(rs.getInt("STORE_ID"));
				store.setStoreName(rs.getString ("STORE_NAME"));
				store.setAddress(rs.getString ("ADDRESS"));
				store.setCity(rs.getString ("CITY"));
				store.setPincode(rs.getString ("PINCODE"));
				store.setState(rs.getString ("STATE"));
				store.setCountry(rs.getString ("COUNTRY"));
				store.setStartDate(rs.getDate ("START_DATE"));
				store.setCloseDate(rs.getDate ("CLOSE_DATE"));
				store.setEmpId(rs.getInt ("EMP_ID"));
				return store;
			}
		});
		return store;
	}
}