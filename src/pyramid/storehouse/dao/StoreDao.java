package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.view.StoreView;

public interface StoreDao {

	public void saveStore (StoreView store);

	public StoreView getStore (int empId);

	public StoreView getStoreByStoreId (int storeId);

	public List<StoreView> getAllStores ();
}
