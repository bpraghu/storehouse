package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.view.EmployeeView;

public class EmployeeDaoImpl extends BaseDao implements EmployeeDao {

	private static Logger logger = LoggerFactory.getLogger(EmployeeDaoImpl.class);

	@Override
	public EmployeeView getEmployee(int empid) {

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("empid", empid);
		EmployeeView emp = null;
		
		try {
			emp = this.getNamedParameterJdbcTemplate().queryForObject("select * from employee where emp_id=:empid", paramMaps, new RowMapper<EmployeeView> () {
	
				@Override
				public EmployeeView mapRow(ResultSet rs, int rowNum) throws SQLException {
	
					EmployeeView emp = new EmployeeView ();
					emp.setEmpId(rs.getInt("EMP_ID"));
					emp.setFirstName(rs.getString("FIRST_NAME"));
					emp.setLastName(rs.getString("LAST_NAME"));
					emp.setEmail(rs.getString("EMAIL"));
					emp.setMobile(rs.getString("MOBILE"));
					emp.setGender(rs.getString("GENDER"));
					emp.setActive(rs.getString("ACTIVE"));
					emp.setType(rs.getString("TYPE"));
					emp.setJoinDate(rs.getDate("JOIN_DATE"));
					emp.setLastDate(rs.getDate("LAST_DATE"));
					emp.setRoleId(rs.getInt("ROLE_ID"));
					return emp;
				}
				
			});
		}
		catch (EmptyResultDataAccessException excep) {
			logger.error ("No employee Found");
		}
		return emp;
	}

	@Override
	public List<EmployeeView> getAllEmployees() {

		List<EmployeeView> empList = this.getNamedParameterJdbcTemplate().query ("select * from employee", new RowMapper<EmployeeView> () {

			@Override
			public EmployeeView mapRow(ResultSet rs, int rowNum) throws SQLException {

				EmployeeView emp = new EmployeeView ();
				emp.setEmpId(rs.getInt("EMP_ID"));
				emp.setFirstName(rs.getString("FIRST_NAME"));
				emp.setLastName(rs.getString("LAST_NAME"));
				emp.setEmail(rs.getString("EMAIL"));
				emp.setMobile(rs.getString("MOBILE"));
				emp.setGender(rs.getString("GENDER"));
				emp.setActive(rs.getString("ACTIVE"));
				emp.setType(rs.getString("TYPE"));
				emp.setJoinDate(rs.getDate("JOIN_DATE"));
				emp.setLastDate(rs.getDate("LAST_DATE"));
				emp.setRoleId(rs.getInt("ROLE_ID"));
				return emp;
			}
			
		});
		return empList;
	}

	@Override
	public List<EmployeeView> getAllEmployees(String colName, String criteria) {

		List<EmployeeView> empList = getAllEmployees (colName, criteria, true);
		return empList;
	}
	
	private String getEmployeeSearchSQL (String colName, String criteria, boolean withoutLoginIDS) {

		String EMP_SEARCH_QUERY = "select e.*, u.USER_NAME from employee e left join user u on e.EMP_ID = u.EMP_ID ";

		if(withoutLoginIDS) {
			EMP_SEARCH_QUERY += "where e.FIRST_NAME like '%" + criteria + "%'";
		}
		else if (colName != null && colName.trim().length() > 0) {
			EMP_SEARCH_QUERY += "where e." + colName + " like '%" + criteria + "%'";
		}
		return EMP_SEARCH_QUERY;
	}

	@Override
	public void saveEmployee(EmployeeView emp) {

		Map<String, Object> data = new HashMap<String, Object> ();

		String SQL_EMP = "INSERT INTO employee " +
				"(`FIRST_NAME`, `LAST_NAME`,`EMAIL`,`MOBILE`,`GENDER`,`ACTIVE`,`TYPE`,`JOIN_DATE`,`ROLE_ID`)" +
				"VALUES (:FIRST_NAME,:LAST_NAME,:EMAIL,:MOBILE,:GENDER,:ACTIVE,:TYPE,:JOIN_DATE,:ROLE_ID)";

		if(emp.getEmpId() > 0) {
			SQL_EMP = "UPDATE pyramid_schema.employee " + 
					 "SET " +  
					 "   FIRST_NAME = :FIRST_NAME " + 
					 "  ,LAST_NAME = :LAST_NAME " + 
					 "  ,EMAIL = :EMAIL " + 
					 "  ,MOBILE = :MOBILE " + 
					 "  ,GENDER = :GENDER " + 
					 "  ,ACTIVE = :ACTIVE " + 
					 "  ,TYPE = :TYPE " + 
					 "  ,JOIN_DATE = :JOIN_DATE " + 
					 "  ,ROLE_ID = :ROLE_ID " + 
					 "WHERE EMP_ID = :EMP_ID";

		}

		data.put("FIRST_NAME", emp.getFirstName());
		data.put("LAST_NAME", emp.getLastName());
		data.put("EMAIL", emp.getEmail());
		data.put("MOBILE", emp.getMobile());
		data.put("GENDER", emp.getGender());
		data.put("ACTIVE", emp.getActive());
		data.put("TYPE", emp.getType());
		data.put("JOIN_DATE", emp.getJoinDate());
		data.put("ROLE_ID", emp.getRoleId());
		data.put("EMP_ID", emp.getEmpId());

		this.getNamedParameterJdbcTemplate().update(SQL_EMP, data);
	}

	@Override
	public EmployeeView findEmployeeByMobile(EmployeeView emp) {

		List<EmployeeView> empList = getAllEmployees ("MOBILE", emp.getMobile(), false);
		if (empList!=null && empList.size() > 0) {
			return empList.get(0);
		}
		return null;
	}

	@Override
	public List<EmployeeView> getUnlinkedEmployees() {

		String SQL = "SELECT * FROM employee WHERE EMP_ID NOT IN (SELECT EMP_ID FROM store where emp_id is not null)";

		List<EmployeeView> empList = this.getNamedParameterJdbcTemplate().query (SQL, new RowMapper<EmployeeView> () {

			@Override
			public EmployeeView mapRow(ResultSet rs, int rowNum) throws SQLException {

				EmployeeView emp = new EmployeeView ();
				emp.setEmpId(rs.getInt("EMP_ID"));
				emp.setFirstName(rs.getString("FIRST_NAME"));
				emp.setLastName(rs.getString("LAST_NAME"));
				emp.setEmail(rs.getString("EMAIL"));
				emp.setMobile(rs.getString("MOBILE"));
				emp.setGender(rs.getString("GENDER"));
				emp.setActive(rs.getString("ACTIVE"));
				emp.setType(rs.getString("TYPE"));
				emp.setJoinDate(rs.getDate("JOIN_DATE"));
				emp.setLastDate(rs.getDate("LAST_DATE"));
				emp.setRoleId(rs.getInt("ROLE_ID"));
				return emp;
			}
			
		});
		return empList;
	}

	@Override
	public List<EmployeeView> getAllEmployees(String colName, String criteria, boolean withoutLoginIDS) {

		String EMP_SEARCH_QUERY = null;
		EMP_SEARCH_QUERY = getEmployeeSearchSQL (colName, criteria, withoutLoginIDS);

		List<EmployeeView> empList = this.getNamedParameterJdbcTemplate().query (EMP_SEARCH_QUERY, new RowMapper<EmployeeView> () {

			@Override
			public EmployeeView mapRow(ResultSet rs, int rowNum) throws SQLException {

				EmployeeView emp = new EmployeeView ();
				emp.setEmpId(rs.getInt("EMP_ID"));
				emp.setFirstName(rs.getString("FIRST_NAME"));
				emp.setLastName(rs.getString("LAST_NAME"));
				emp.setEmail(rs.getString("EMAIL"));
				emp.setMobile(rs.getString("MOBILE"));
				emp.setGender(rs.getString("GENDER"));
				emp.setActive(rs.getString("ACTIVE"));
				emp.setType(rs.getString("TYPE"));
				emp.setJoinDate(rs.getDate("JOIN_DATE"));
				emp.setLastDate(rs.getDate("LAST_DATE"));
				emp.setRoleId(rs.getInt("ROLE_ID"));
				
				emp.setUserName(rs.getString("USER_NAME"));

				return emp;
			}
			
		});
		return empList;
	}
}
