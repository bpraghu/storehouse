package pyramid.storehouse.dao;

import java.util.Date;
import java.util.List;

import pyramid.storehouse.view.StockView;

public interface StockDao {

	public void stockEntry (StockView stock);
	
	public void stockBalanceUpdate (StockView stock);
	
	public void stockUpdate (StockView stock);

	public List<StockView> getAllStockUpdates(int storeID);

	public StockView getStockUpdateByStoreAndStockID(int storeID, int itemID);

	public List<StockView> getStockTransactions(int storeID, int itemID, Date fromDate, Date tillDate, String operation);

	public StockView getStockTransaction(Long stockID);
}
