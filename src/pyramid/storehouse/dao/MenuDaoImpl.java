package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.pojo.Menu;

public class MenuDaoImpl extends BaseDao implements MenusDao {

	private static int MAIN_MENUS_ID = 0; 

	@Override
	public List<Menu> getAllMenus() {

		List<Menu> mainMenus = getAllMenus (MAIN_MENUS_ID);

		for (Menu mainMenu : mainMenus) {
			mainMenu.setSubMenus(getAllMenus (mainMenu.getMenuId()));
		}
		return mainMenus;
	}

	@Override
	public List<Menu> getAllMenus(int parentId) {

		String SQL_MENU_SELECT = "SELECT * FROM MENUS WHERE PARENT_MENU_ID=:PARENT_MENU_ID ORDER BY SORT_ORDER ASC";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("PARENT_MENU_ID", parentId);

		List<Menu> menus = this.getNamedParameterJdbcTemplate().query(SQL_MENU_SELECT, paramMap, new RowMapper<Menu>() {

			@Override
			public Menu mapRow(ResultSet rs, int rowNum) throws SQLException {

				Menu menu = new Menu ();
				menu.setMenuId(rs.getInt("MENU_ID"));
				menu.setMenuName(rs.getString("MENU_NAME"));
				menu.setParentMenuId(rs.getInt("PARENT_MENU_ID"));
				menu.setUrl(rs.getString("URL"));
				return menu;
			}
			
		});
		return menus;
	}

	@Override
	public List<Menu> getAllMenusForRole(int roleId) {

		String SQL_MENU_SELECT = "{call GET_MENU_PERMISSIONS_BY_ROLEID (:roleID) }";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleID", roleId);

		List<Menu> menus = this.getNamedParameterJdbcTemplate().query(SQL_MENU_SELECT, paramMap, new RowMapper<Menu>() {

			@Override
			public Menu mapRow(ResultSet rs, int rowNum) throws SQLException {

				Menu menu = new Menu ();
				menu.setMenuId(rs.getInt("MENU_ID"));
				menu.setMenuName(rs.getString("MENU_NAME"));
				menu.setPermitted(rs.getInt("ID")!=0);
				return menu;
			}
		});
		return menus;
	}
}
