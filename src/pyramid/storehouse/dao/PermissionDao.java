package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.pojo.Menu;
import pyramid.storehouse.pojo.Permission;
import pyramid.storehouse.view.PermissionView;

public interface PermissionDao {

	public List<Permission> getAllPermission (int roleId);

	public void updatePermission (PermissionView permission);
	
	public void updatePermission (int roleId, Menu menu);
}
