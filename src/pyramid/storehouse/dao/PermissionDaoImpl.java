package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.pojo.Menu;
import pyramid.storehouse.pojo.Permission;
import pyramid.storehouse.view.PermissionView;

public class PermissionDaoImpl extends BaseDao implements PermissionDao {

	@Override
	public List<Permission> getAllPermission(int roleId) {

		String SQL_PERMISSON_SELECT = "select * from permission where ROLE_ID=:ROLE_ID";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ROLE_ID", roleId);

		List<Permission> perms = this.getNamedParameterJdbcTemplate().query(SQL_PERMISSON_SELECT, params, new RowMapper<Permission>() {

			@Override
			public Permission mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Permission p = new Permission (rs.getInt("ID"), rs.getInt("ROLE_ID"), rs.getInt("MENU_ID"));
				return p;
			}
		});
		return perms;
	}

	@Override
	public void updatePermission(PermissionView permission) {
		String SQL_PERMISSION_UPDATE = "{call UPDATE_PERMISSION (:roleID,:menuID, :enable)}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleID", permission.getRoleId());
		paramMap.put("menuID", permission.getMenuId());
		paramMap.put("enable", permission.isPermitted()?1:0);

		this.getNamedParameterJdbcTemplate().update(SQL_PERMISSION_UPDATE, paramMap);
	}

	@Override
	public void updatePermission(int roleId, Menu menu) {
		String SQL_PERMISSION_UPDATE = "{call UPDATE_PERMISSION (:roleID,:menuID, :enable)}";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleID", roleId);
		paramMap.put("menuID", menu.getMenuId());
		paramMap.put("enable", menu.isPermitted()?1:0);

		this.getNamedParameterJdbcTemplate().update(SQL_PERMISSION_UPDATE, paramMap);
	}
}
