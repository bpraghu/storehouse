package pyramid.storehouse.dao;

import java.util.List;

import pyramid.storehouse.view.ItemView;

public interface ItemDao {

	public List<ItemView> getAllItems();

	public ItemView getItem (int itemID);

	public ItemView getItem (String itemName);

	public boolean addItem (ItemView item);
	
	public boolean updateItem(ItemView item);
}
