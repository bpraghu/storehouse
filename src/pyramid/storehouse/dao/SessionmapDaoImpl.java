package pyramid.storehouse.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;

import org.springframework.jdbc.core.RowMapper;

import pyramid.storehouse.pojo.Sessionmap;
import pyramid.storehouse.util.SessionUtil;
import pyramid.storehouse.view.UserView;

public class SessionmapDaoImpl extends BaseDao implements SessionmapDao {

	@Override
	public Sessionmap createSession(UserView user) {

		String INSERT_SESSION_MAP = "INSERT INTO sessionmap (`CLIENT_KEY`, `LOGGED_IN`, `LAST_ACCESSED`, `USER_NAME`,`ACTIVE`) "
				+ "VALUES (:CLIENT_KEY, :LOGGED_IN, :LAST_ACCESSED, :USER_NAME, :ACTIVE)";

		String clientKey = getKey ();
		
		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("CLIENT_KEY", clientKey);
		paramMaps.put("LOGGED_IN", Calendar.getInstance().getTime());
		paramMaps.put("LAST_ACCESSED", Calendar.getInstance().getTime());
		paramMaps.put("USER_NAME", user.getUserName());
		paramMaps.put("ACTIVE", 1);

		this.getNamedParameterJdbcTemplate().update(INSERT_SESSION_MAP, paramMaps);
		
		return getSessionmap(clientKey);
	}

	
	
	@Override
	public Sessionmap getSessionmap(String clientKey) {

		String SELECT_SESSION_SQL = "select * from sessionmap where client_key=:clientKey order by ID desc limit 1;";
		
		HashMap<String, String> paramMaps = new HashMap <String, String>();
		paramMaps.put("clientKey", clientKey);
		Sessionmap sessionMap = this.getNamedParameterJdbcTemplate().queryForObject(SELECT_SESSION_SQL, paramMaps, new RowMapper<Sessionmap>() {

			@Override
			public Sessionmap mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				
				Sessionmap sessionMap = new Sessionmap();
				sessionMap.setClientKey(rs.getString("CLIENT_KEY"));
				sessionMap.setLoggedIn(rs.getDate("LOGGED_IN"));
				sessionMap.setLoggedOut(rs.getDate("LOGGED_OUT"));
				sessionMap.setLastAccessed(rs.getDate("LAST_ACCESSED"));
				sessionMap.setUserName(rs.getString("USER_NAME"));
				sessionMap.setActive(rs.getInt("ACTIVE"));
				return sessionMap;
			}
		});
		return sessionMap;
	}

	private String getKey () {
		return SessionUtil.getSession().getId();
	}

	@Override
	public void deactivateSession(String clientKey) {

		String UPDATE_SESSION_MAP = "UPDATE sessionmap SET ACTIVE = 2, LAST_ACCESSED = :LAST_ACCESSED, LOGGED_OUT = :LOGGED_OUT  WHERE CLIENT_KEY=:CLIENT_KEY";

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("CLIENT_KEY", clientKey);
		paramMaps.put("LAST_ACCESSED", Calendar.getInstance().getTime());
		paramMaps.put("LOGGED_OUT", Calendar.getInstance().getTime());
		this.getNamedParameterJdbcTemplate().update(UPDATE_SESSION_MAP, paramMaps);
	}



	@Override
	public Sessionmap updateLastAccessedSession (String clientKey) {

		String updateSQL = "UPDATE pyramid_schema.sessionmap " + 
				 "SET " + 
				 "  LAST_ACCESSED = :LAST_ACCESSED" +  
				 " WHERE CLIENT_KEY = :CLIENT_KEY";
		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("CLIENT_KEY", clientKey);
		paramMaps.put("LAST_ACCESSED", Calendar.getInstance().getTime());
		this.getNamedParameterJdbcTemplate().update(updateSQL, paramMaps);
		return null;
	}

	

	@Override
	public void cleanAbandonedSessions() {

		HashMap<String, Object> paramMaps = new HashMap <String, Object>();
		paramMaps.put("TIME_INTERVAL", 1);
		int result = this.getNamedParameterJdbcTemplate().update("{CALL SESSION_CLEANER (:TIME_INTERVAL) }", paramMaps);
	}
}
