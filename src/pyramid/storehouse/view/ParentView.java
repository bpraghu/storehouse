package pyramid.storehouse.view;

import java.io.Serializable;

public class ParentView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2957754887045021843L;
	private String action;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
