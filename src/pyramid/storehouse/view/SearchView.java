package pyramid.storehouse.view;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import pyramid.storehouse.service.SearchService;
import pyramid.storehouse.service.StockService;

@ManagedBean
@ViewScoped
public class SearchView {

	private String colName;
	private String searchCriteria;
	private List<EmployeeView> emps;
	private List<SupplierView> suppliers;
	private Integer storeId;
	private String operation;
	private Integer itemId;
	private Date entryDate;
	
	@ManagedProperty("#{stockService}")
	private StockService stockService;

	@ManagedProperty("#{searchService}")
	private SearchService searchService;

	public String getColName() {
		return colName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getSearchCriteria() {
		return searchCriteria;
	}
	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public void submitEmpSearch () {

		System.out.println (" In searching ");
		emps = searchService.submitEmpSearch(this.colName, this.searchCriteria);
	}
	
	public void submitSupplierSearch () {

		suppliers = searchService.submitSupplierSearch(this.colName, this.searchCriteria);
	}

	public List<EmployeeView> getEmps() {

		return emps;
	}

	public void setEmps(List<EmployeeView> emps) {
		this.emps = emps;
	}
	public SearchService getSearchService() {
		return searchService;
	}
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	public List<SupplierView> getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(List<SupplierView> suppliers) {
		this.suppliers = suppliers;
	}
	
	private List<StockView> transList;
	
	public void submitTranSearch () {
		
		System.out.println (storeId + "	" + operation + " "  + itemId + " " + entryDate);
		transList = stockService.getTransactions(this);
	}
	public List<StockView> getTransList() {
		return transList;
	}
	public void setTransList(List<StockView> transList) {
		this.transList = transList;
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	public StockService getStockService() {
		return stockService;
	}
	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public void setTransactionListSearch (String operation) {
		System.out.println ("In Set transaction List " + operation);
		this.operation = "I";
	}
}
