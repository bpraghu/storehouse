package pyramid.storehouse.view;

import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import pyramid.storehouse.pojo.Menu;

@ManagedBean(name="menus")
public class DynamicMenus {
	
	private MenuModel model;
	List<Menu> menus;

	public DynamicMenus () {
		
	}
	
	public MenuModel getModel() {
		return model;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
		
		model = new DefaultMenuModel();
		Menu logoutMenu = null;

		for (Menu menu : menus) {

			if(menu.getMenuName().equalsIgnoreCase("logout")) {
				logoutMenu = menu;
				continue;
			}

			DefaultSubMenu row = new DefaultSubMenu(menu.getMenuName());
			model.addElement(row);
			List<Menu> subMenus = menu.getSubMenus();
			
			for (Menu subMenu : subMenus) {
				row.addElement(new DefaultMenuItem(subMenu.getMenuName(), null, subMenu.getUrl()));
			}
		}
		
		if(logoutMenu != null) {
			DefaultMenuItem defaultMenuItem = new DefaultMenuItem(logoutMenu.getMenuName(), null, logoutMenu.getUrl());
			model.addElement(defaultMenuItem);
		}
	}
}
