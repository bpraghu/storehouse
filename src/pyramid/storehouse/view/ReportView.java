package pyramid.storehouse.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.report.pojo.StockData;
import pyramid.storehouse.service.ReportService;
import pyramid.storehouse.service.StockService;
import pyramid.storehouse.service.StoreService;
import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.util.JSFUtil;

@ManagedBean(name = "reportView")
@SessionScoped
public class ReportView {

	private Logger logger = Logger.getLogger(ReportView.class);

	private Date fromDate;
	private Date endDate;

	private String formattedFromDate;
	private String formattedEndDate;

	private int month;
	private int storeId;
	private int itemId;
	private String itemName;

	@ManagedProperty("#{reportService}")
	private ReportService reportService;

	@ManagedProperty("#{storeService}")
	private StoreService storeService;

	@ManagedProperty("#{stockService}")
	private StockService stockService;

	private List<StockData> stockData;

	private List<ReportDataView> stockBalanceData;

	@PostConstruct
	public void init() {

		stockData = new ArrayList<StockData>();
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
		formattedFromDate = DateUtil.formatIST(this.fromDate);
	}

	public String getFormattedFromDate() {

		return formattedFromDate;
	}

	public String getFormattedEndDate() {
		return formattedEndDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		if (endDate != null) {
			this.formattedEndDate = DateUtil.formatIST(this.endDate);
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeID) {
		this.storeId = storeID;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemID) {
		this.itemId = itemID;
	}

	public String getStoreName() {
		String storeName = "";
		if (storeId != 0) {
			storeName = storeService.getStore(storeId).getStoreName();
		}
		return storeName;
	}

	public String showStoreWiseItemReport() {
		String view = "storeReport";
		try {
			if(this.getEndDate() == null) {
				this.endDate = DateUtil.addDay(Calendar.getInstance().getTime(), -1);
				view = "openingBalance";
			}
			else {
				this.endDate = DateUtil.setCurrentTimeStamp (this.getEndDate());
			}
			stockBalanceData = reportService.generateStoreWiseItemReport(this);
		}
		catch (Exception e) {
			logger.error("An error occurred while generating showStoreWiseItemReport", e);
			RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("An error ooccurred while generating the report, please contact Admin..."));
		}
			
		return view;
	}
	
	public String showStockUpdateForStoreByDateReport() {
		String view = "stockUpdateReport";
		try {
			if(stockData != null) {
				stockData.clear();
			}
			
			if(this.getEndDate() == null) {
				this.setEndDate(DateUtil.setCurrentTimeStamp (Calendar.getInstance().getTime()));
			}
			else {
				this.setEndDate(DateUtil.setCurrentTimeStamp (this.getEndDate()));
			}
			stockBalanceData = reportService.generateStockUpdateForStoreReport(this);

			StockData newStockData = null;

			int prevItemID = 0;
			
			if (stockBalanceData == null || stockBalanceData.size() == 0) {
				JSFUtil.showMessage("No data was found for the dates between " + getFormattedFromDate() + " and " + getFormattedEndDate());
				RequestContext.getCurrentInstance().addCallbackParam("dataAvailable", false);
				return null;
			}

			// Using this logic to add a dummy row in the report to show consumed items entry just for aesthetics reason
			int set = 0;			

			for (ReportDataView rptData : stockBalanceData) {
				if (prevItemID == rptData.getItemID()) {
					newStockData.addStock(rptData);
					set = 0;
				} else {
					if (set == 1) {
						ReportDataView prevRptData = newStockData.getStockReportData().get(0);
						ReportDataView newRptData = new ReportDataView(prevRptData.getItemID(), prevRptData.getItemName(), AppConstants.OUTGOING_STOCK, 0, prevRptData.getVolume()); 
						newStockData.addStock(newRptData);
						set = 0;
					}
					newStockData = new StockData(AppConstants.NEW_STOCK, rptData.getItemName());
					this.stockData.add(newStockData);
					newStockData.addStock(rptData);
					set = 1;
				}
				prevItemID = rptData.getItemID();
			}
			
			if (set == 1) {
				ReportDataView prevRptData = newStockData.getStockReportData().get(0);
				ReportDataView newRptData = new ReportDataView(prevRptData.getItemID(), prevRptData.getItemName(), AppConstants.OUTGOING_STOCK, 0, prevRptData.getVolume()); 
				newStockData.addStock(newRptData);
			}
		}
		catch (Exception e) {
			logger.error("An error occurred while generating showStockUpdateForStoreByDateReport", e);
			RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("An error ooccurred while generating the report, please contact Admin..."));
		}
			
		return view;
	}

	public void showItemWiseReportDialog() {
		String view = "itemReportDialog.xhtml";
		Map<Integer, StockView> storeStockUpdate = stockService.getStoreStockUpdate();
		stockData.clear();

		List<ReportDataView> stockDataTemp = null;
		List<ReportDataView> openingStockData = null;

		try {
			ReportView rptQuery = new ReportView();
			rptQuery.setStoreId(this.getStoreId());
			// The opening balance is calculated till -1 day from the FromDate.
			rptQuery.setEndDate(DateUtil.addDay(this.getFromDate(), -1));
			openingStockData = reportService.generateStoreWiseItemReport(rptQuery);
		}
		catch (Exception e) {
			logger.error("An error occurred while fetching opening balance ", e);
		}

		try {
			stockDataTemp = reportService.generateItemWiseReport(this);
			StockData newStockData = null;
			StockData dispersedStockData = null;

			int prevItemID = 0;
			
			if (stockDataTemp == null || stockDataTemp.size() == 0) {
				JSFUtil.showMessage("No data was found for the dates between " + getFormattedFromDate() + " and " + getFormattedEndDate());
				return;
			}

			for (ReportDataView rptData : stockDataTemp) {
				if (prevItemID == rptData.getItemID()) {
					if (rptData.getOperation().equals("I")) {
						newStockData.addStock(rptData);
					} else {
						dispersedStockData.addStock(rptData);
					}
				} else {
					newStockData = new StockData(AppConstants.NEW_STOCK, rptData.getItemName());
					dispersedStockData = new StockData(AppConstants.DISPERSED_STOCK, rptData.getItemName());

					StockView currentStock = storeStockUpdate.get(Integer.valueOf(rptData.getItemID()));

					long stockUpdate = 0;
					long stockVolumeUpdate = 0;

					if (currentStock != null) {
						stockUpdate = currentStock.getQuantity();
						stockVolumeUpdate = currentStock.getVolume();
					}
					dispersedStockData.setCurrentStockBalance(stockUpdate);
					dispersedStockData.setCurrentVolumeBalance(stockVolumeUpdate);

					setOpeningBalanceForItem(dispersedStockData, openingStockData, rptData.getItemID());

					this.stockData.add(newStockData);
					this.stockData.add(dispersedStockData);

					if (rptData.getOperation().equals("I")) {
						newStockData.addStock(rptData);
					} else {
						dispersedStockData.addStock(rptData);
					}
				}
				prevItemID = rptData.getItemID();
			}

			Map<String, Object> options = new HashMap<String, Object>();
			options.put("modal", true);
			options.put("width", "1800");
			options.put("height", "980");
			options.put("contentWidth", "100%");
			options.put("contentHeight", "100%");

			RequestContext.getCurrentInstance().openDialog(view, options, null);
		} catch (Exception e) {
			logger.error("An error ooccurred while generating the report", e);
			JSFUtil.showErrorMessage("An error ooccurred while generating showItemWiseReportDialog, please contact Admin...");
		}
	}

	private void setOpeningBalanceForItem (StockData dispersedStockData, List<ReportDataView> openingStockData, int itemID) {

		Long openingStockBalance = 0l;
		Long openingStockVolBalance = 0l;

		for(ReportDataView rptDataView : openingStockData) {
			if(rptDataView.getItemID() == itemID) {
				openingStockBalance = rptDataView.getQuantity();
				openingStockVolBalance = rptDataView.getVolume();
				break;
			}
		}
		dispersedStockData.setOpeningStockBalance(openingStockBalance);
		dispersedStockData.setOpeningVolumeBalance(openingStockVolBalance);
	}
	
	@Override
	public String toString() {
		return "ReportQuery [fromDate=" + fromDate + ", endDate=" + endDate + ", month=" + month + ", storeID="
				+ storeId + ", itemID=" + itemId + "]";
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public List<StockData> getStockData() {
		return stockData;
	}

	public void setStockData(List<StockData> stockData) {
		this.stockData = stockData;
	}

	public StoreService getStoreService() {
		return storeService;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public List<ReportDataView> getStockBalanceData() {
		return stockBalanceData;
	}

	public void setStockBalanceData(List<ReportDataView> stockBalanceData) {
		this.stockBalanceData = stockBalanceData;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
}
