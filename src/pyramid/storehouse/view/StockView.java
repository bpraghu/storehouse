package pyramid.storehouse.view;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.primefaces.context.RequestContext;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.service.ItemService;
import pyramid.storehouse.service.StockService;
import pyramid.storehouse.service.StoreService;
import pyramid.storehouse.service.SupplierService;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.util.RequestUtil;
import pyramid.storehouse.util.SessionUtil;

@ManagedBean(name="stock")
public class StockView implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3255104603134093866L;
	private Long stockId;
	private Long supplierId;
	private String supplierName;
	private Integer itemId;
	private Map<String, String> items;
	private List<ItemView> itemsList; 
	private List<StockView> trans; 
	private Map<String, String> suppliers; 
	// Used only for Showing Stocks
	private String itemName;
	private String employeeId;
	private Integer storeId;
	private Integer toStoreId;
	private Integer fromStoreId;
	private String toStoreName;
	private String fromStoreName;
	private String operation;
	private String unit;

	private Long quantity;
	private Long volume;

	private String driverMobile;

	private String vehicleRegNum;
	private Date entryDate;
	private Date modifyDate;
	private String remarks;
	private String stockUpdate;
	
	private boolean showVolume;
	
	@ManagedProperty("#{itemService}")
	private ItemService itemService;

	@ManagedProperty("#{supplierService}")
	private SupplierService supplierService;

	@ManagedProperty("#{stockService}")
	private StockService stockService;
	
	@ManagedProperty("#{storeService}")
	private StoreService storeService;

	private List<StockView> transList;

	private Map<String, String> storesMap;
	private Map<String, String> allStoresMap;

	@PostConstruct
	public void loadItems () {
		items = itemService.getItemsMap();
		itemsList = itemService.getItems();
		suppliers = supplierService.getSupplierNames();
		setAndFilterCurrentStore();
	}
	
	private void setAndFilterCurrentStore() {

		List<StoreView> stores = storeService.getAllStores();
		storesMap = new HashMap<String, String>();
		allStoresMap = new HashMap<String, String>();
		StoreView loggedInStore = (StoreView)SessionUtil.getSessionData(AppConstants.STORE);

		for(StoreView store : stores) {
			if (!loggedInStore.getStoreName().equalsIgnoreCase(store.getStoreName())) {
				storesMap.put(store.getStoreName(), String.valueOf(store.getStoreId()));
			}
			allStoresMap.put(store.getStoreName(), String.valueOf(store.getStoreId()));
		}
	}

	/*public List<ItemView> completeItem (String query) {
		
		List<ItemView> filteredItems = new ArrayList<ItemView>(2);
		
		for (ItemView item : items) {
			if(item.getName().startsWith(query)) {
				filteredItems.add(item);
			}
		}
		return filteredItems;
	}*/
	
	/*public List<SupplierView> completeSupplier (String query) {
		
		List<SupplierView> filteredSuppliers = new ArrayList<SupplierView>(2);

		for (SupplierView supplier : suppliers) {
			if(supplier.getSupplierName().startsWith(query) || supplier.getSupplierName().startsWith(query.toLowerCase()) || supplier.getSupplierName().startsWith(query.toUpperCase())) {
				filteredSuppliers.add(supplier);
			}
		}
		return filteredSuppliers;
	}
*/
	

	public void getStockUpdateFromDB () {
		
		System.out.println ("==================== get Stock Update 23 ========================== ");
		StockView stock = stockService.getStockUpdate(this);
		
		if (stock!=null) {  
			setStockUpdate(stock.getQuantity() != null?stock.getQuantity():Long.valueOf(0), stock.getVolume()!=null?stock.getVolume():Long.valueOf(0), stock.getUnit());
		}

		setVolumeVisibility ();
	}
	
	private void setVolumeVisibility () {
		
		for (ItemView item : itemsList) {
			if(item.getItemId() == itemId) {
				setShowVolume (item.getUnit().indexOf("+")!=-1);
				break;
			}
		}
	}
	
	/*public void getStockUpdateFromDB (String stockID) {
		
		System.out.println ("==================== get Stock Update ========================== ");

		StockView tempStock = new StockView();
		tempStock.setItemId(stockID);
		StockView stock = stockService.getStockUpdate(tempStock);
		setStockUpdate(stock.getQuantity());
		System.out.println (stock);
	}*/
	
	public StockView() {
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Date getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public String getDriverMobile() {
		return driverMobile;
	}

	public void setDriverMobile(String driverMobile) {
		this.driverMobile = driverMobile;
	}

	public String getVehicleRegNum() {
		return vehicleRegNum;
	}

	public void setVehicleRegNum(String vehicleRegNum) {
		this.vehicleRegNum = vehicleRegNum;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public void reset () {

		supplierId = null;
		supplierName = null;
		itemId = null;
		employeeId = null;
		storeId = null;
		operation = null;
		quantity = null;
		driverMobile = null;
		vehicleRegNum = null;
		entryDate = null;
		modifyDate = null;
		remarks = null;
	}

	public String save () {
		String view = "stock.xhtml";
		boolean stockEntryStatus = false;
		setVolume (RequestUtil.getLong("supplierForm:volume"));

		try {
			stockEntryStatus = stockService.saveStock (this);
		}
		catch (Exception e) {
			stockEntryStatus = false;
		}

		if (stockEntryStatus) {
			JSFUtil.addMessage("New Stock successfully saved!");
			view = JSFUtil.getRedirectURL(view);
		}
		else {
			JSFUtil.addErrorMessage("New Stock addition failed, please contact admin!");
		}
		return view;
	}


	public boolean isStockAvailable () {
		
		StockView stock = stockService.getStockUpdate(this);
		long stockNos = stock.getQuantity();
		long vol = stock.getVolume();
		long requestedQuantity = this.getQuantity();
		long requestedVol = 0;
		if(getVolume() != null) {
			requestedVol = getVolume();
		}

		return requestedQuantity <= stockNos && requestedVol <= vol;
	}
	
	public String disperse () {

		String view = "disperse.xhtml";
		
		boolean stockAvailable = false;

		setVolume (RequestUtil.getLong("supplierForm:volume"));
		stockAvailable = isStockAvailable();
		
		
		if (!stockAvailable) {
			JSFUtil.addErrorMessage("Not enough stock !");
			return view;
		}
		
		boolean stockDispersionStatus = false;
		
		try {
			stockDispersionStatus = stockService.saveStockDisperse(this);
		}
		catch (Exception e) {
			stockDispersionStatus = false;
		}

		if (stockDispersionStatus) {
			JSFUtil.addMessage("Stock dispersed successfully!");
			view = JSFUtil.getRedirectURL(view);
		}
		else {
			JSFUtil.addErrorMessage("Stock dispersion failed, please contact admin!");
		}
		return view;
	}

	public void editStock () {
	//	String view = "stock.xhtml";
		
		System.out.println (stockId);
		StockView stock = stockService.getTransaction(this);
		
		if(stock!=null) {
			setItemId(stock.getItemId());
			setOperation(stock.getOperation());
			setQuantity(stock.getQuantity());
			setVolume(stock.getVolume());
			setVolumeVisibility ();
			setSupplierId(stock.getSupplierId());
			setDriverMobile(stock.getDriverMobile());
			setVehicleRegNum(stock.getVehicleRegNum());
			setRemarks(stock.getRemarks());
			setToStoreId(stock.getToStoreId());
			setFromStoreId(stock.getFromStoreId());

			if (stock.getFromStoreId() == null && stock.getToStoreId() == null) {
				if(AppConstants.OUTGOING_STOCK.equalsIgnoreCase(getOperation())) {
				//	view = "disperse.xhtml";
					RequestContext.getCurrentInstance().addCallbackParam("OPERATION", AppConstants.OUTGOING_STOCK);
				}
				else {
					RequestContext.getCurrentInstance().addCallbackParam("OPERATION", AppConstants.INCOMING_STOCK);
				}
			}
			else {
				if(stock.getToStoreId() != 0) {
					RequestContext.getCurrentInstance().addCallbackParam("OPERATION", AppConstants.OUTGOING_STOCK_TRANSFER);
				}
				else {
					RequestContext.getCurrentInstance().addCallbackParam("OPERATION", AppConstants.INCOMING_STOCK_TRANSFER);
				}
			}
		}
		/*	return view;
		}
		else {
			JSFUtil.showErrorMessage("The selected transaction ID not found");
			view = "transList.xhtml";
		}
		return view;*/
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ItemService getItemService() {
		return itemService;
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	public SupplierService getSupplierService() {
		return supplierService;
	}

	public void setSupplierService(SupplierService supplierService) {
		this.supplierService = supplierService;
	}

	@Override
	public String toString() {
		return "StockView [stockId=" + stockId + ", supplierId=" + supplierId + ", supplierName=" + supplierName
				+ ", itemId=" + itemId + ", itemName=" + itemName
				+ ", employeeId=" + employeeId + ", storeId=" + storeId + ", operation=" + operation + ", quantity="
				+ quantity + ", driverMobile=" + driverMobile + ", vehicleRegNum=" + vehicleRegNum + ", entryDate="
				+ entryDate + ", modifyDate=" + modifyDate + ", remarks=" + remarks + ", itemService=" + itemService
				+ "]";
	}

	public Map<String, String> getItems() {
		return items;
	}

	public void setItems(Map<String, String> items) {
		this.items = items;
	}

	public Map<String, String> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(Map<String, String> suppliers) {
		this.suppliers = suppliers;
	}

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public void setStockUpdate(long stockUpdate, long volume, String unit) {
		this.stockUpdate = "Current stock = " + stockUpdate;
		if(unit.length() == 2) {
			this.stockUpdate += " Current volume = " + volume; 
		}
	}

	public String getStockUpdate() {
		return stockUpdate;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public boolean isShowVolume() {
		return showVolume;
	}

	public void setShowVolume(boolean showVolume) {
		this.showVolume = showVolume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public List<StockView> getTrans() {
		return trans;
	}

	public void setTrans(List<StockView> trans) {
		this.trans = trans;
	}

	public List<StockView> getTransList() {
		return transList;
	}

	public void disperseEditSave() {
		
		setVolume (RequestUtil.getLong("stockDisperseForm:volume"));
		boolean status = stockService.saveEditStockDisperse(this);

		if (status) {
			JSFUtil.addMessage("Transaction changed successfully!");
		}
		else {
			JSFUtil.addErrorMessage("Transaction change failed! Please contact admin..");
		}
		
		System.out.println ("Disperse Edit save called ");
	}

	public void disperseTransferEditSave() {
		
		setVolume (RequestUtil.getLong("transferEditForm:volume"));
		boolean status = stockService.saveEditStockDisperse(this);

		if (status) {
			JSFUtil.addMessage("Transaction changed successfully!");
		}
		else {
			JSFUtil.addErrorMessage("Transaction change failed! Please contact admin..");
		}
		
		System.out.println ("Disperse Edit save called ");
	}

	
	public void saveEditedStock() {
		
		setVolume (RequestUtil.getLong("stockEditForm:volume"));
		boolean status = stockService.saveEditedStock(this);
		if (status) {
			JSFUtil.addMessage("Transaction changed successfully!");
		}
		else {
			JSFUtil.addErrorMessage("Transaction change failed! Please contact admin..");
		}
		System.out.println (this.toString());
		System.out.println ("save Edit save called ");
	}
	
	public void saveTransferEditedStock() {
		
		setVolume (RequestUtil.getLong("transferReceiveForm:volume"));
		boolean status = stockService.saveEditedStock(this);
		if (status) {
			JSFUtil.addMessage("Transaction changed successfully!");
		}
		else {
			JSFUtil.addErrorMessage("Transaction change failed! Please contact admin..");
		}
		System.out.println (this.toString());
		System.out.println ("save Transfer Edited save called ");
	}

	public String transfer() {

		String view = "transfer.xhtml";
		boolean transferStatus = false;
		setVolume (RequestUtil.getLong("transferForm:volume"));
		
		boolean stockAvailable = false;
		stockAvailable = isStockAvailable();

		if (!stockAvailable) {
			JSFUtil.addErrorMessage("Not enough stock !");
			return view;
		}
		
		transferStatus = stockService.saveStockDisperse(this);
		System.out.println ("Transfer save called ");
		if (transferStatus) {
			JSFUtil.addMessage("Stock transfered successfully!");
			view = JSFUtil.getRedirectURL(view);
		}
		else {
			JSFUtil.addErrorMessage("Stock transfer failed, please contact admin!");
		}
		return view;
	}
	
	public String transferReceive() {

		String view = "transferReceive.xhtml";
		boolean transferStatus = false;
		setVolume (RequestUtil.getLong("transferReceiveForm:volume"));
		transferStatus = stockService.saveStock(this);
		System.out.println ("Transfer Receive save called ");
		if (transferStatus) {
			JSFUtil.addMessage("Stock transfer received successfully!");
			view = JSFUtil.getRedirectURL(view);
		}
		else {
			JSFUtil.addErrorMessage("Stock transfer receive failed, please contact admin!");
		}
		return view;
	}
	
	public StoreService getStoreService() {
		return storeService;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public Map<String, String> getStoreMapsMinusPresentStore () {

		return storesMap;
	}

	public Map<String, String> getStoresMap() {
		return storesMap;
	}

	public void setStoresMap(Map<String, String> storesMap) {
		this.storesMap = storesMap;
	}

	public Integer getFromStoreId() {
		return fromStoreId;
	}

	public void setFromStoreId(Integer fromStoreId) {
		this.fromStoreId = fromStoreId;
	}

	public Integer getToStoreId() {
		return toStoreId;
	}

	public void setToStoreId(Integer toStoreId) {
		this.toStoreId = toStoreId;
	}

	public Map<String, String> getAllStoresMap() {
		return allStoresMap;
	}

	public void setAllStoresMap(Map<String, String> allStoresMap) {
		this.allStoresMap = allStoresMap;
	}

	public String getFromStoreName() {
		return fromStoreName;
	}

	public void setFromStoreName(String fromStoreName) {
		this.fromStoreName = fromStoreName;
	}

	public String getToStoreName() {
		return toStoreName;
	}

	public void setToStoreName(String toStoreName) {
		this.toStoreName = toStoreName;
	}
}
