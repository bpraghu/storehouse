package pyramid.storehouse.view;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.context.RequestContext;

import pyramid.storehouse.util.JSFUtil;

@ManagedBean(name="transferReportView")
public class TransferView extends ReportView {

	private Date transferDate;
	private String storeName;
	private Long quantity;
	private Long volume;
	private String remark;
	private List<TransferView> transferList;

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public void showTransferFromReport () {

		try {
			transferList = getReportService().generateTransferReport(this);
			if (transferList == null || transferList.size() == 0) {
				RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("No transfers during this period"));
				RequestContext.getCurrentInstance().addCallbackParam("dataAvailable", "false");
				return;
			}
			RequestContext.getCurrentInstance().addCallbackParam("dataAvailable", "true");
		}
		catch (Exception e) {
			RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("An error ooccurred while generating the report, please contact Admin..."));
		}
	}
	
	public void showTransferReceivedReport () {

		System.out.println ("In showTransferReceivedReport ===== ");
		try {
			transferList = getReportService().generateTransferToReport(this);
			if (transferList == null || transferList.size() == 0) {
				RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("No transfers during this period"));
				RequestContext.getCurrentInstance().addCallbackParam("dataAvailable", "false");
				return;
			}
			RequestContext.getCurrentInstance().addCallbackParam("dataAvailable", "true");
		}
		catch (Exception e) {
			RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("An error ooccurred while generating the report, please contact Admin..."));
		}
	}

	public String getFromStoreName () {
		return super.getStoreName();
	}
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<TransferView> getTransferList() {
		return transferList;
	}

	public void setTransferList(List<TransferView> transferList) {
		this.transferList = transferList;
	}

	
}
