package pyramid.storehouse.view;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.service.EmployeeService;
import pyramid.storehouse.service.MainService;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.util.SessionUtil;
import pyramid.storehouse.vo.BaseForm;

@ManagedBean(name="UserView")
public class UserView extends BaseForm implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8429634007435014820L;
	
	private static Logger logger = LoggerFactory.getLogger(UserView.class);
	
	private StrongPasswordEncryptor spe = new StrongPasswordEncryptor();

	@ManagedProperty("#{mainService}")
	private MainService mainService;

	@ManagedProperty("#{employeeService}")
	private EmployeeService empService;

	@ManagedProperty("#{menus}")
	private DynamicMenus menus;

	private int empId;
	private String userName;

	private String oldPassword;
	private String password;
	private String confirmPassword;
	private boolean active;
	private int roleId;
	private int storeId;

	public UserView() {
	}

	public UserView( String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public int getEmpId() {
		return this.empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String authenticate() {

		String uri = "login.xhtml";

		if (getUserName().trim().length() > 0 && getPassword().trim().length() > 0) {
			
			try {
				if (mainService.authenticate(this)) {
					uri = "landing.xhtml";
				}
				else {
					FacesMessage msg = new FacesMessage("Incorrect Login or Password ", "ERROR MSG");
			        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}
			}
			catch (Exception e) {
				
				logger.error("An exception occured during authentication ", e);
				FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
		        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		}
		return uri;
	}

	public String logout() {

		mainService.logout();
		JSFUtil.addMessage("You have been successfully logged out! Please login again..");
		return JSFUtil.getRedirectURL("login.xhtml");
	}

	public void clearSession() {

		mainService.logout();
	}


	public MainService getMainService() {
		return mainService;
	}

	public void setMainService(MainService mainService) {
		this.mainService = mainService;
	}

	public DynamicMenus getMenus() {
		return menus;
	}

	public void setMenus(DynamicMenus menus) {
		this.menus = menus;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void submit() {

		if (!password.equals(confirmPassword)) {
			JSFUtil.addErrorMessage("Passwords do not match"); 
		}

        HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String employeeId = request.getParameter("empId");
        empId = Integer.parseInt(employeeId);

        if (empService.isUserNameAlreadyExisting(this)) {
        	JSFUtil.addErrorMessage("Userid already registered! please try a different user name..");
        }
        else {
			boolean status = empService.createLogin(this);

			if(status) {
				JSFUtil.addMessage("Login user created successfully! ");
				RequestContext context = RequestContext.getCurrentInstance();
				context.closeDialog("user.xhtml");
			}
			else {
				JSFUtil.addErrorMessage("Login user creation failed! ");
			}
        }
	}
	
	public String changePassword () {

		String view = "changePassword.xhtml";

		if (!password.equals(confirmPassword)) {
			JSFUtil.addErrorMessage("New Passwords do not match");
			return view;
		}

		UserView loggedInUser = (UserView) SessionUtil.getSessionData(AppConstants.USER_DATA);

		// Check old password
	//	UserView user = empService.getUser(this);
	//	if (!oldPassword.equals(loggedInUser.getPassword())) {
		if (!spe.checkPassword(oldPassword, loggedInUser.getPassword())) {
			JSFUtil.addErrorMessage("Old password is incorrect ! ");
			return view;
		}

        empId = loggedInUser.getEmpId();

		boolean status = empService.changePassword(this);

		if(status) {
			JSFUtil.addMessage("Password changed successfully! ");
			view = JSFUtil.getRedirectURL(view);
		}
		else {
			JSFUtil.addErrorMessage("Password change failed! Please contact admin ");
		}
		return view;
	}
	
	public String resetPassword () {

		String view = "passwordReset.xhtml";

		if (!password.equals(confirmPassword)) {
			JSFUtil.addErrorMessage("New Passwords do not match");
			return view;
		}

        empId = (Integer)SessionUtil.getSessionData(AppConstants.EMPID_FOR_PASSWORD_CHANGE);

		boolean status = empService.changePassword(this);

		if(status) {
			JSFUtil.addMessage("Password changed successfully! ");
			RequestContext.getCurrentInstance().closeDialog("user.xhtml");
		}
		else {
			JSFUtil.addErrorMessage("Password change failed! Please contact admin ");
		}
		SessionUtil.removeData (AppConstants.EMPID_FOR_PASSWORD_CHANGE);
		return view;
	}

	public void showUserDialog() {
		Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 650);
        options.put("height", 450);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");

        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put("empId", Arrays.asList(String.valueOf(empId)));        

        RequestContext.getCurrentInstance().openDialog("user.xhtml", options, params);
	}

	public void showPasswordResetDialog() {
		Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 650);
        options.put("height", 400);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");

        SessionUtil.store(AppConstants.EMPID_FOR_PASSWORD_CHANGE, empId);

        Map<String, List<String>> params = new HashMap<String, List<String>>();
        RequestContext.getCurrentInstance().openDialog("passwordReset.xhtml", options, params);
	}
	
	public EmployeeService getEmpService() {
		return empService;
	}

	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
}
