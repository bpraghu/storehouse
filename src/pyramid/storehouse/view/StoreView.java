package pyramid.storehouse.view;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pyramid.storehouse.service.EmployeeService;
import pyramid.storehouse.service.StoreService;
import pyramid.storehouse.util.JSFUtil;

@ManagedBean(name="store")
public class StoreView implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -414709712109397831L;

	private static final Logger logger = LoggerFactory.getLogger(StoreView.class);

	private int storeId;
	private String action;

	private String storeName;

	private String address;
	private String city;
	private String state;
	private String country;

	private String pincode;

	private Date startDate;
	private Date closeDate;
	
	private int empId;
	private String empName;
	private List<StoreView> stores;
	private Map<String, String> storesMap;

	@ManagedProperty("#{storeService}")
	private StoreService storeService;

	@ManagedProperty("#{employeeService}")
	private EmployeeService empService;

	private List<EmployeeView> emps;

	public StoreView() {
		action = "New";
		country = "India";
	}

	@PostConstruct
	public void init () {

	}

	public StoreView(int storeId, String storeName, String address, Date startDate) {
		this.storeId = storeId;
		this.storeName = storeName;
		this.address = address;
		this.startDate = startDate;
	}

	public StoreView(int storeId, String storeName, String address, String city,
			String pincode, Date startDate, Date closeDate) {
		this.storeId = storeId;
		this.storeName = storeName;
		this.address = address;
		this.city = city;
		this.pincode = pincode;
		this.startDate = startDate;
		this.closeDate = closeDate;
	}

	public int getStoreId() {
		return this.storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return this.storeName;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getCloseDate() {
		return this.closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public void reset() {
		this.storeName = null;
		this.address = null;
		this.city = null;
		this.pincode = null;
		this.startDate = null;
		this.closeDate = null;
	}

	public String save () {
		String view = "store.xhtml";
		boolean stockEntryStatus = false;

		try {
			storeService.storeSave (this);
			stockEntryStatus = true;
		}
		catch (Exception e) {
			stockEntryStatus = false;
			logger.error("An error occurred while trying to add/update a store, data is " + this.toString(), e);
		}

		if (stockEntryStatus) {
			if (this.storeId == 0) {
				JSFUtil.addMessage("New Store successfully created!");
			}
			else {
				JSFUtil.addMessage("Store successfully updated!");
			}
			view = JSFUtil.getRedirectURL(view);
		}
		else {
			if (this.storeId == 0) {
				JSFUtil.addErrorMessage("New Store addition failed, please contact admin!");
			}
			else {
				JSFUtil.addErrorMessage("Store updation failed, please contact admin!");
			}
		}
		return view;
	}

	public void onEmployeeSelect(SelectEvent event) {
        empName = ((EmployeeView)event.getObject()).getFirstName();
        empId = ((EmployeeView)event.getObject()).getEmpId();
	}
	
	
	public String onStoreSelect() {
        StoreView store = storeService.getStore(storeId);
        this.storeId = store.getStoreId();
        this.storeName = store.getStoreName();
        this.city = store.getCity();
        this.address = store.getAddress();
        this.empId = store.getEmpId();
        this.empName = store.getEmpName();
        this.state = store.getState();
        this.pincode = store.getPincode();
        this.startDate = store.getStartDate();

        this.action = "Edit ";
        return "store.xhtml";
	}

	
	@Override
	public String toString() {
		return "Store [storeId=" + storeId + ", storeName=" + storeName
				+ ", address1=" + address + ", city=" + city + ", state=" + state + ", country="
				+ country + ", pincode=" + pincode + ", startDate=" + startDate
				+ ", closeDate=" + closeDate + "]";
	}

	public StoreService getStoreService() {
		return storeService;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public List<StoreView> getStores() {

		stores = storeService.getAllStores();
		storesMap = new HashMap<String, String>();

		for(StoreView store : stores) {
			storesMap.put(store.getStoreName(), String.valueOf(store.getStoreId()));
		}
		return stores;
	}

	public void setStores(List<StoreView> stores) {
		this.stores = stores;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public EmployeeService getEmpService() {
		return empService;
	}

	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}

	public List<EmployeeView> getEmps() {
		if(emps == null)
			emps = empService.getUnlinkedEmployees();
		return emps;
	}

	public void setEmps(List<EmployeeView> emps) {
		this.emps = emps;
	}

	public Map<String, String> getStoresMap() {
		if (storesMap == null) {
			getStores ();
		}
		return storesMap;
	}

	public void setStoresMap(Map<String, String> storesMap) {
		this.storesMap = storesMap;
	}
}
