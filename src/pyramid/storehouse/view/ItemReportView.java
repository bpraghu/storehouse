package pyramid.storehouse.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import pyramid.storehouse.report.pojo.StockData;
import pyramid.storehouse.service.ReportService;
import pyramid.storehouse.service.StockService;
import pyramid.storehouse.service.StoreService;
import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.util.RequestUtil;

@ManagedBean(name = "itemReportView")
@RequestScoped
public class ItemReportView extends ReportView {

	Logger logger = Logger.getLogger(ReportView.class);

	private Date fromDate;
	private Date endDate;

	private String formattedFromDate;
	private String formattedEndDate;

	private int month;
	private int storeId;
	private int itemId;

	@ManagedProperty("#{reportService}")
	private ReportService reportService;

	@ManagedProperty("#{storeService}")
	private StoreService storeService;

	@ManagedProperty("#{stockService}")
	private StockService stockService;

	private List<StockData> stockData;

	private List<ReportDataView> stockBalanceData;

	@PostConstruct
	public void init() {

		stockData = new ArrayList<StockData>();
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
		formattedFromDate = DateUtil.formatIST(this.fromDate);
	}

	public String getFormattedFromDate() {

		return formattedFromDate;
	}

	public String getFormattedEndDate() {
		return formattedEndDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		if (endDate != null) {
			this.formattedEndDate = DateUtil.formatIST(this.endDate);
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeID) {
		this.storeId = storeID;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemID) {
		this.itemId = itemID;
	}

	public String getStoreName() {
		String storeName = "";
		if (storeId != 0) {
			storeName = storeService.getStore(storeId).getStoreName();
		}
		return storeName;
	}

	public String showStoreWiseItemReport() {
		String view = "storeReport";
		boolean isOpeningBalanceReport = (RequestUtil.getParameter("openingBalance") !=null && RequestUtil.getParameter("openingBalance").equalsIgnoreCase("openingBalance"));

		try {
			if(isOpeningBalanceReport) {
				this.endDate = DateUtil.addDay(Calendar.getInstance().getTime(), -1);
				formattedEndDate = DateUtil.formatIST(this.endDate);
				view = "openingBalance";
			}
			else {
				this.endDate = DateUtil.setCurrentTimeStamp (this.getEndDate());

				if(this.getEndDate() == null) {
					formattedEndDate = DateUtil.formatIST(Calendar.getInstance().getTime());
				}
			}
			stockBalanceData = reportService.generateStoreWiseItemReport(this);
		}
		catch (Exception e) {
			logger.error("An error occurred while generating showStoreWiseItemReport", e);
			RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage("An error ooccurred while generating the report, please contact Admin..."));
		}
			
		return view;
	}
	
	@Override
	public String toString() {
		return "ReportQuery [fromDate=" + fromDate + ", endDate=" + endDate + ", month=" + month + ", storeID="
				+ storeId + ", itemID=" + itemId + "]";
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public List<StockData> getStockData() {
		return stockData;
	}

	public void setStockData(List<StockData> stockData) {
		this.stockData = stockData;
	}

	public StoreService getStoreService() {
		return storeService;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public List<ReportDataView> getStockBalanceData() {
		return stockBalanceData;
	}

	public void setStockBalanceData(List<ReportDataView> stockBalanceData) {
		this.stockBalanceData = stockBalanceData;
	}
}
