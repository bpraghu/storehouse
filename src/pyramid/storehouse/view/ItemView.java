package pyramid.storehouse.view;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.service.ItemService;
import pyramid.storehouse.util.JSFUtil;

@ManagedBean(name="item")
public class ItemView extends ParentView implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6318936578465528336L;
	private int itemId;
	private String name;
	private String unit;
	private Date dateAdded;
	private List<ItemView> items;
	private Map<String, String> itemsMap; 

	@ManagedProperty("#{itemService}")
	private ItemService itemService;

	public ItemView() {
		setAction(AppConstants.NEW);
	}

	@PostConstruct
	public void init () {
		itemsMap = itemService.getItemsMap();
	}

	public ItemView(int itemId, Date dateAdded) {
		this.itemId = itemId;
		this.dateAdded = dateAdded;
	}

	public ItemView(int itemId, String name, Date dateAdded) {
		this.itemId = itemId;
		this.name = name;
		this.dateAdded = dateAdded;
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public Date getDateAdded() {
		return this.dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void reset () {
		this.itemId = 0;
		this.name = null;
		this.unit = null;
		this.dateAdded = null;
	}

	public String save () {
		
		String returnURL = "item.xhtml";
		boolean status = false;

		if(itemId == 0) {
			
			boolean duplicateName = itemService.isItemNameAlreadyPresent(getName());
			
			if(!duplicateName) {		
				status = itemService.itemAdd(this);
			}
			else {
				JSFUtil.addErrorMessage("Item name already exists! Please try a different name ..");
			}
		}
		else {
			status = itemService.itemUpdate(this);
		}
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		if(status) {
			returnURL = JSFUtil.getRedirectURL(returnURL);
		}
		return returnURL;
	}
	
	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", name=" + name + ", unit=" + unit
				+ ", dateAdded=" + dateAdded + "]";
	}

	public ItemService getItemService() {
		return itemService;
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	public List<ItemView> getItems() {
		
		if (items == null) {
			items = itemService.getItems();
		}
		return items;
	}

	public void setItems(List<ItemView> items) {
		this.items = items;
	}

	public String onItemSelect () {
		String view = "item.xhtml";
		ItemView item = itemService.getItem(itemId);
		
		this.itemId = item.getItemId();
		this.name = item.getName();
		this.unit = item.getUnit();
		setAction(AppConstants.EDIT);
		return view;
	}

	public Map<String, String> getItemsMap() {
		return itemsMap;
	}

	public void setItemsMap(Map<String, String> itemsMap) {
		this.itemsMap = itemsMap;
	}
}
