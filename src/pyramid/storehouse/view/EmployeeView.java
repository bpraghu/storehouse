package pyramid.storehouse.view;


import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.springframework.format.annotation.DateTimeFormat;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.pojo.Role;
import pyramid.storehouse.service.EmployeeService;
import pyramid.storehouse.service.RoleService;
import pyramid.storehouse.util.JSFUtil;

@ManagedBean(name="employee")
public class EmployeeView extends ParentView implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3033866321645757416L;
	private int empId;
	private int roleId;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private String gender;
	private String active;
	private String type;
	private Date joinDate;
	private String roleName;
	
	private String userName;

	private List<EmployeeView> emps;
	
	@ManagedProperty("#{employeeService}")
	private EmployeeService empService;
	
	@ManagedProperty("#{roleService}")
	private RoleService roleService;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date lastDate;

	@ManagedProperty("#{searchView}")
	private SearchView searchView;

	public EmployeeView() {
		setAction(AppConstants.NEW);
	}

	public EmployeeView(int empId, String firstName, String lastName,
			String mobile, Date joinDate) {
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobile = mobile;
		this.joinDate = joinDate;
	}

	public EmployeeView(int empId, Role role, String firstName, String lastName,
			String email, String mobile, String gender, String active,
			String type, Date joinDate, Date lastDate) {
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.active = active;
		this.type = type;
		this.joinDate = joinDate;
		this.lastDate = lastDate;
	}

	@PostConstruct
	public void loadEmployees () {
		System.out.println (" Loading employees ");
		emps = empService.getEmployees();
	}

	public int getEmpId() {
		return this.empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getJoinDate() {
		return this.joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public Date getLastDate() {
		return this.lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public void reset () {
		this.empId = 0;
		this.firstName = null;
		this.lastName = null;
		this.email = null;
		this.mobile = null;
		this.gender = null;
		this.active = null;
		this.type = null;
		this.joinDate = null;
		this.lastDate = null;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public String save () {

		boolean status = false;
		String returnURL = "emp.xhtml";
		if (this.getEmpId() == 0) {
			active = "A";
			status = empService.employeeSave(this);
		}
		else {
			status = empService.employeeEditSave(this);
			
			if (status) {
				this.reset();
				JSFUtil.addMessage("Employee successfully updated!");
			} else {
				JSFUtil.addErrorMessage("Employee updation failed, please contact admin!");
			}
		}
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		if(status) {
			returnURL = JSFUtil.getRedirectURL(returnURL);
		}
		return returnURL;
	}

	
	public void onRoleSelect(SelectEvent event) {
        roleName = ((Role)event.getObject()).getRoleName();
        roleId = ((Role)event.getObject()).getRoleId();
	}

	public String onEmployeeSelect() {

		EmployeeView emp = empService.getEmployee(this.empId);
		this.empId = emp.getEmpId();
		this.firstName = emp.firstName;
		this.lastName = emp.lastName;
		this.mobile = emp.mobile;
		this.email = emp.email;
		this.gender = emp.gender;
		this.type = emp.type;
		this.active = emp.active==null?"A":emp.active;
		this.joinDate = emp.joinDate;
		this.roleId = emp.getRoleId();
		this.roleName = this.getRoleService().getRole(this.roleId).getRoleName();
		
		setAction(AppConstants.EDIT);
		
		return "emp.xhtml";
	}
	
	public void updateUserActiveStatus () {

		EmployeeView emp = empService.getEmployee(this.empId);
		if(emp.getActive() == null || emp.getActive().equals(AppConstants.ACTIVE)) {
			emp.setActive(AppConstants.INACTIVE);
		}
		else {
			emp.setActive(AppConstants.ACTIVE);
		}
		boolean status = empService.updateEmployeeStatus (emp);
		searchView.submitEmpSearch();

		if(status){
			JSFUtil.addMessage("User " + emp.firstName + " status has been updated ");
		}
		else {
			JSFUtil.addErrorMessage("User " + emp.firstName + " status update failed ");
		}
	
	}


	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", roleId=" + roleId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", mobile=" + mobile + ", gender=" + gender + ", active=" + active + ", type="
				+ type + ", joinDate=" + joinDate + ", roleName=" + roleName + ", empController=" + empService
				+ ", lastDate=" + lastDate + "]";
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public EmployeeService getEmpService() {
		return empService;
	}

	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}

	public List<EmployeeView> getEmps() {
		return emps;
	}

	public void setEmps(List<EmployeeView> emps) {
		this.emps = emps;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public SearchView getSearchView() {
		return searchView;
	}

	public void setSearchView(SearchView searchView) {
		this.searchView = searchView;
	}
}
