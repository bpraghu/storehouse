package pyramid.storehouse.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

import pyramid.storehouse.dao.RoleDao;
import pyramid.storehouse.pojo.Menu;
import pyramid.storehouse.pojo.Role;
import pyramid.storehouse.service.PermissionsService;
import pyramid.storehouse.util.JSFUtil;

@ManagedBean(name="permissionView")
@ViewScoped
public class PermissionView {

	private int roleId;
	private boolean permitted;
	private int menuId;

	private Map<String, String> rolesMap;

	@ManagedProperty("#{roleDao}")
	private RoleDao roleDao;

	@ManagedProperty("#{permissionsService}")
	private PermissionsService permissionService;

	private List<Menu> permissions;
	
	private String[] selectedPerms;

	@PostConstruct
	public void init () {

		List<Role> roles = roleDao.getAllRoles();
		rolesMap = new HashMap<String, String> ();

		for (Role role : roles) {
			rolesMap.put(role.getRoleName(), String.valueOf(role.getRoleId()));
		}
	}

	public void fetchRolePermissions () {

		permissions = permissionService.getAllMenusForRole(roleId);
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Map<String, String> getRolesMap() {
		return rolesMap;
	}

	public void setRolesMap(Map<String, String> rolesMap) {
		this.rolesMap = rolesMap;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public PermissionsService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionsService permissionService) {
		this.permissionService = permissionService;
	}

	public List<Menu> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Menu> permissions) {
		this.permissions = permissions;
	}

	public boolean isPermitted() {
		return permitted;
	}

	public void setPermitted(boolean permitted) {
		this.permitted = permitted;
	}

	public void updatePermissions (AjaxBehaviorEvent event) {
		
		SelectBooleanCheckbox permit = (SelectBooleanCheckbox) event.getComponent();
	    permitted = (Boolean) permit.getValue();
        menuId = (Integer) permit.getAttributes().get("selectedMenuID");

        boolean status = permissionService.updatePermission(this);

        if (status) {
        	JSFUtil.addMessage("Permission updated!");
        }
        else {
        	JSFUtil.addErrorMessage("Permission updated failed!");
        }
        fetchRolePermissions ();
	}
	
	public void submitPermissions () {
		
		boolean status = false;
		List<Menu> latestMenuPerms = permissionService.getAllMenusForRole(roleId);
		
		for (Menu menu : permissions) {
			int index = latestMenuPerms.indexOf(menu);
			
			if(index != -1) {
				Menu dbCopy = latestMenuPerms.get(index);
				if(dbCopy.isPermitted() != menu.isPermitted()) {
			        status = permissionService.updatePermission(this.roleId, menu);
				}
			}			
		}

        if (status) {
        	JSFUtil.addMessage("Permission updated!");
        }
        else {
        	JSFUtil.addErrorMessage("Permission updated failed!");
        }
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public String[] getSelectedPerms() {
		return selectedPerms;
	}

	public void setSelectedPerms(String[] selectedPerms) {
		this.selectedPerms = selectedPerms;
	}
}
