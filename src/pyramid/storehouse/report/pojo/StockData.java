package pyramid.storehouse.report.pojo;

import java.util.ArrayList;
import java.util.List;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.view.ReportDataView;

public class StockData {

	private List<ReportDataView> stockReportData;
	private long quantity;
	private long volume;
	private String type;
	private String item;
	private long currentStockBalance;
	private long openingStockBalance;
	
	private long currentVolumeBalance;
	private long openingVolumeBalance;

	public StockData () {

	}

	public StockData (String type) {

		this.type = type;
		stockReportData = new ArrayList <ReportDataView> ();
	}
	
	public StockData (String type, String itemName) {

		this.type = type;
		this.item = itemName;
		stockReportData = new ArrayList <ReportDataView> ();
	}

	public void addStock (ReportDataView rptData) {

		stockReportData.add(rptData);
		if(rptData.getOperation().equalsIgnoreCase(AppConstants.OUTGOING_STOCK)) {
			quantity += (-1 * rptData.getQuantity());
			volume += (-1 * rptData.getVolume());
		}
		else {
			quantity += rptData.getQuantity();
			volume += rptData.getVolume();
		}
	}

	public long getQuantity() {
		return Math.abs(quantity);
	}

	public List<ReportDataView> getStockReportData() {
		return stockReportData;
	}

	public void setStockReportData(List<ReportDataView> stockReportData) {
		this.stockReportData = stockReportData;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public long getCurrentStockBalance() {
		return currentStockBalance;
	}

	public void setCurrentStockBalance(long currentStockBalance) {
		this.currentStockBalance = currentStockBalance;
	}

	public long getOpeningStockBalance() {
		return openingStockBalance;
	}

	public void setOpeningStockBalance(long openingStockBalance) {
		this.openingStockBalance = openingStockBalance;
	}

	public long getOpeningVolumeBalance() {
		return openingVolumeBalance;
	}

	public void setOpeningVolumeBalance(long openingVolumeBalance) {
		this.openingVolumeBalance = openingVolumeBalance;
	}

	public long getCurrentVolumeBalance() {
		return currentVolumeBalance;
	}

	public void setCurrentVolumeBalance(long currentVolumeBalance) {
		this.currentVolumeBalance = currentVolumeBalance;
	}

	public long getVolume() {
		return Math.abs(volume);
	}

	public void setVolume(long volume) {
		this.volume = volume;
	}
}