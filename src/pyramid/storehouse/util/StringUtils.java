package pyramid.storehouse.util;

public class StringUtils {

	public static boolean isEmpty(String anyString) {
		return anyString == null || anyString.equals("");
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}
}
