package pyramid.storehouse.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

	public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

	public static String getParameter (String param) {

		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(param);
	}
	
	public static Long getLong (String param) {

		String value = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(param);
		if(value != null) {
			return Long.valueOf(value);
		}
		return null;
	}
}
