package pyramid.storehouse.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {

	public static TimeZone getIndiaTimeZone () {

		TimeZone tsobe = TimeZone.getTimeZone("IST");
		TimeZone.setDefault(tsobe);	
		return tsobe;
	}

	public static Date getCurrentDate () {
		
		TimeZone tsobe = DateUtil.getIndiaTimeZone ();
		Calendar cal = Calendar.getInstance(tsobe, Locale.ENGLISH);
		return cal.getTime();
	}

	public static String getMonthQuery (int month) {

		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd");
		TimeZone tsobe = DateUtil.getIndiaTimeZone ();
		Calendar cal = Calendar.getInstance (tsobe, Locale.ENGLISH);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DATE, 1);
		String startDt = sdf.format(cal.getTime());
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		String endDt = sdf.format(cal.getTime());
		return "'" + startDt + "' AND '" + endDt + "'";
	}

	public static String formatWithTimeStamp (Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	
	public static String format (Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	
	public static String formatIST (Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat ("dd-MMMMM-yyyy");
		return sdf.format(date);
	}

	public static String getMonthName (int month) {
	
		String monthName = "";
		switch (month) {
			case 1:
				monthName = "January";
			break;
			case 2:
				monthName = "February";
				break;
			case 3:
				monthName = "March";
				break;
			case 4:
				monthName = "April";
				break;
			case 5:
				monthName = "May";
				break;
			case 6:
				monthName = "June";
				break;
			case 7:
				monthName = "July";
				break;
			case 8:
				monthName = "August";
				break;
			case 9:
				monthName = "September";
				break;
			case 10:
				monthName = "October";
				break;
			case 11:
				monthName = "November";
				break;
			case 12:
				monthName = "December";
		}
		return monthName;
	}

	public static Date setCurrentTimeStamp (Date dt) {

		if (dt != null) {
			Calendar currentTime = Calendar.getInstance();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			cal.set(Calendar.HOUR_OF_DAY, currentTime.get(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, currentTime.get(Calendar.MINUTE));
			cal.set(Calendar.SECOND, currentTime.get(Calendar.SECOND));
			
			dt = cal.getTime();
		}
		return dt;
	}

	public static Date addDay (Date paramDate, int noOfDays) {

		if (paramDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(paramDate);
			cal.add(Calendar.DATE, noOfDays);
			paramDate = DateUtil.setEODTimeStamp(cal.getTime());
		}
		return paramDate;
	}

	public static Date setBODTimeStamp (Date dt) {

		if (dt != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			cal.set(Calendar.HOUR_OF_DAY, 00);
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);

			dt = cal.getTime();
		}
		return dt;
	}
	
	public static Date setEODTimeStamp (Date dt) {

		if (dt != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);

			dt = cal.getTime();
		}
		return dt;
	}


	public static Date convertStringToDate (String date) {

		SimpleDateFormat sdf = new SimpleDateFormat ("dd-MM-yyy");
		Date convertedDate = null;
		try {
			convertedDate = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedDate;
	}

	public static void main(String[] args) {

		/*SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd");
		System.out.println (sdf.format(new Date ()));
		
		System.out.println (addDay(new Date(), -1));
		
		System.out.println (DateUtil.addDay(Calendar.getInstance().getTime(), -1));*/
		
		Date dt = new Date ();
		
		System.out.println(DateUtil.setBODTimeStamp(dt));
		System.out.println(DateUtil.setEODTimeStamp(dt));
		
		System.out.println (convertStringToDate("23-11-2009"));
	}
}
