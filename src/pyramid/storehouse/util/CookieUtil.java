package pyramid.storehouse.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {

	public static void deleteCookie(String cookieName) {

		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpServletResponse httpResp = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
		
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals(cookieName)) {
					cookies[i].setMaxAge(0);
					cookies[i].setValue(null);
					cookies[i].setPath(null);
					httpResp.addCookie(cookies[i]);
				}
			}
		}
	}

	public static String getValueFromCookie(String CookieName) {

		String cookieValue = null;
		Cookie cookie = getCookie(null, CookieName);
		if (cookie != null) {
			cookieValue = cookie.getValue();
		}
		return cookieValue;
	}
	
	public static String getValueFromCookie(HttpServletRequest httpReq, String CookieName) {

		String cookieValue = null;
		Cookie cookie = getCookie(httpReq, CookieName);
		if (cookie != null) {
			cookieValue = cookie.getValue();
		}
		return cookieValue;
	}

	public static Cookie getCookie(HttpServletRequest httpReq, String CookieName) {
		
		if(httpReq == null) {
			httpReq = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		}

		Cookie cookie = null;
		if (httpReq != null) {
			Cookie[] cookies = httpReq.getCookies();
			if (cookies != null) {
				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals(CookieName)) {
						cookie = cookies[i];
						break;
					}
				}
			}
		}
		return cookie;
	}

	public static void addSessionCookieToResponse(String CookieName,
			String CookieValue, boolean isSecure) {

		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
		addSessionCookieToResponse(request, response, CookieName, CookieValue, null, isSecure);
	}

	public static void addSessionCookieToResponse(HttpServletRequest request, HttpServletResponse response, String CookieName,
			String CookieValue, String DomainName, boolean isSecure) {
		addCookieToResponse(request, response, CookieName, CookieValue, null, -1, isSecure);
	}

	public static void addCookieToResponse(HttpServletRequest request, HttpServletResponse response, String CookieName,
			String CookieValue, int cookieAge, boolean isSecure) {
		addCookieToResponse(request, response, CookieName, CookieValue, null, cookieAge, isSecure);
	}

	public static void addCookieToResponse(HttpServletRequest request, HttpServletResponse response, String CookieName,
			String CookieValue, String DomainName, int cookieAge, boolean isSecure) {

		if ((CookieName != null) && (CookieValue != null)) {
			Cookie aCookie = new Cookie(CookieName, CookieValue);
			aCookie.setMaxAge(cookieAge);

			aCookie.setPath("/");
			if (DomainName != null)
				aCookie.setDomain(DomainName);
			response.addCookie(aCookie);
		}
	}
}
