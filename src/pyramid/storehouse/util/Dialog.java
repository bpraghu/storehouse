package pyramid.storehouse.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;

public class Dialog {

	private Map<String,Object> options;
    Map<String, List<String>> params;

	public Dialog (boolean modal, int width, int height) {
		options = new HashMap<String, Object>();
        options.put("modal", modal);
        options.put("width", width);
        options.put("height", height);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");
	}

	public void addParam (String paramName, String value) {

		params.put(paramName, Arrays.asList(value));
	}

	public void showUserDialog(String view) {

        RequestContext.getCurrentInstance().openDialog(view, options, params);
	}
}
