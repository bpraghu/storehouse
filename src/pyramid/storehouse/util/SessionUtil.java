package pyramid.storehouse.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtil {

	public static Object getSessionData(String key) {
        HttpSession session = getSession();
        Object data = null;
        if (session != null) {
        	data = session.getAttribute(key);
        }
        return data;
	}
	
	public static void removeData(String key) {
        HttpSession session = getSession();
        if (session != null) {
        	session.removeAttribute(key);
        }
	}

	public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

	public static HttpSession getNewSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
 
    public static String getUserName() {
        HttpSession session = getSession ();
        return session.getAttribute("username").toString();
    }
 
    public static void store (String key, Object data) {
    	HttpSession session = getSession ();
    	if (session != null) {
    		session.setAttribute(key, data);
    	}
    }
    
    public static String getUserId() {
        HttpSession session = getSession();
        if (session != null)
            return (String) session.getAttribute("userid");
        else
            return null;
    }
}
