package pyramid.storehouse.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

public class JSFUtil {

	public static void addMessage (String msg) {

		addMessage (msg, FacesMessage.SEVERITY_INFO);
	}
	
	public static void addErrorMessage (String msg) {

		addMessage (msg, FacesMessage.SEVERITY_ERROR);
	}

	private static void addMessage (String msg, FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, msg ,null));
	}

	public static String getRedirectURL (String url) {
		url = url + "?faces-redirect=true";
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		return url;
	}
	
	public static String getSessionKey () {
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String ck = request.getParameter("ck");
		return ck;
	}

	public static FacesMessage getErrorMessage (String msg) {
		FacesMessage faceMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);
		return faceMsg;
	}
	
	public static FacesMessage getMessage (String msg) {
		FacesMessage faceMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
		return faceMsg;
	}

	public static void showErrorMessage (String msg) {
		RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getErrorMessage (msg));
	}
	
	public static void showMessage (String msg) {
		RequestContext.getCurrentInstance().showMessageInDialog(JSFUtil.getMessage (msg));
	}
}
