package pyramid.storehouse.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pyramid.storehouse.dao.StoreDao;
import pyramid.storehouse.view.EmployeeView;
import pyramid.storehouse.view.StoreView;

@Service
public class StoreService {

	private Logger logger = LoggerFactory.getLogger(StoreService.class.getName());

	@Autowired
	private StoreDao storeDao;

	@Autowired
	private EmployeeService empService;

	@Transactional
	public void storeSave (StoreView store) {

		logger.debug(" Adding / Updating store ");
		storeDao.saveStore(store);
	}

	public StoreView getStore (int storeId) {

		StoreView store = storeDao.getStoreByStoreId (storeId);
		if(store.getEmpId() != 0) {
			EmployeeView emp = empService.getEmployee(store.getEmpId());
			if (emp != null) {
				store.setEmpName(emp.getFirstName() + " " + emp.getLastName());
			}
		}
		return store;
	}

	public List<StoreView> getAllStores () {

		List<StoreView> stores = storeDao.getAllStores();
		return stores;
	}

	public StoreDao getStoreDao() {
		return storeDao;
	}

	public void setStoreDao(StoreDao storeDao) {
		this.storeDao = storeDao;
	}

	public EmployeeService getEmpService() {
		return empService;
	}

	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}

}
