package pyramid.storehouse.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;

import pyramid.storehouse.dao.ItemDao;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.view.ItemView;

@Service
@ApplicationScoped
public class ItemService {

	private static Logger logger = LoggerFactory.getLogger(ItemService.class);
	
	@Autowired
	private ItemDao itemDao;
	private Map<String, String> itemUnits;
	
	@PostConstruct
	public void init () {
	
		itemUnits = new HashMap<String, String>();
		itemUnits.put("W", "Weight");
		itemUnits.put("Q", "Quantity");
		itemUnits.put("C", "Cubic Feet");
		itemUnits.put("QC", "Quantity + CFT");
		itemUnits.put("WC", "Weight + CFT");
	}

	public boolean itemAdd (ItemView item) {

		boolean status = itemDao.addItem(item);
		if (status) {
			JSFUtil.addMessage("New Item successfully created!");
		}
		else {
			JSFUtil.addErrorMessage("New Item creation failed, please contact admin!");
		}
		return status;
	}

	public List<ItemView> getItems () {

		List<ItemView> items = itemDao.getAllItems();
		
		for (ItemView item : items) {
			item.setUnit(itemUnits.get(item.getUnit()));
		}
		return items;
	}
	
	public Map<String, String> getItemsMap () {

		List<ItemView> items = itemDao.getAllItems();
		Map<String, String> itemMaps = new HashMap<String, String>();
		
		for (ItemView item : items) {
			itemMaps.put(item.getName(), String.valueOf(item.getItemId()));
		}
		return itemMaps;
	}

	public List<String> getItemNames () {

		List<ItemView> items = itemDao.getAllItems();
		List<String> itemNames = new ArrayList<String> (5);
		for (ItemView item : items) {
			itemNames.add(item.getName());
		}
		return itemNames;
	}
	
	public ItemView getItem (int itemId) {

		ItemView item = itemDao.getItem(itemId);
		return item;
	}
	
	public boolean isItemNameAlreadyPresent (String itemName) {

		ItemView item = null;
		
		try {
			item = itemDao.getItem(itemName);
		}
		catch (IncorrectResultSizeDataAccessException ide) {
			logger.error("An error occurred while fetching item by item name " + itemName, ide);
		}
		catch (Exception e) {
			logger.error("An error occurred while fetching item by item name " + itemName, e);
		}
		return item != null;
	}

	public boolean itemUpdate (ItemView item) {

		boolean status = itemDao.updateItem(item);
		if (status) {
			JSFUtil.addMessage("Item successfully updated!");
		}
		else {
			JSFUtil.addErrorMessage("Item update failed, please contact admin!");
		}
		return status;
	}
}
