package pyramid.storehouse.service;

import java.util.List;
import java.util.Properties;

import javax.faces.bean.ApplicationScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pyramid.storehouse.dao.EmployeeDao;
import pyramid.storehouse.dao.RoleDao;
import pyramid.storehouse.dao.SupplierDao;
import pyramid.storehouse.pojo.Role;
import pyramid.storehouse.view.EmployeeView;
import pyramid.storehouse.view.SupplierView;

@Service
@ApplicationScoped
public class SearchService {

	private Properties empColMappings;
	private Properties roleColMappings;
	private Properties supplierColMappings;

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private SupplierDao supplierDao;

	public List<EmployeeView> submitEmpSearch (String colName, String searchCriteria) {

		Properties props = getEmpColMappings();
		colName = props.getProperty(colName, null);
		List<EmployeeView> empList = employeeDao.getAllEmployees(colName, searchCriteria, false);
		
		for (EmployeeView emp : empList) {
			emp.setRoleName(roleDao.getRole(emp.getRoleId()).getRoleName());
		}
		return empList;
	}
	
	@RequestMapping(value="/submitRoleSearch", method=RequestMethod.POST)
	public ModelAndView submitRoleSearch (String colName, String searchCriteria, Model model) {

		colName = getRoleColMappings().getProperty(colName);
		List<Role> roleList = roleDao.getAllRoles();

		model.addAttribute("ROLES", roleList);
		return new ModelAndView("roleSearch");
	}

	public List<SupplierView> submitSupplierSearch (String colName, String searchCriteria) {

		colName = getSupplierColMappings().getProperty(colName);
		List<SupplierView> supplierList = supplierDao.getAllSupplier(colName, searchCriteria);
		return supplierList;
	}

	public Properties getRoleColMappings() {
		return roleColMappings;
	}

	public void setRoleColMappings(Properties roleColMappings) {
		this.roleColMappings = roleColMappings;
	}

	public Properties getEmpColMappings() {
		return empColMappings;
	}

	public void setEmpColMappings(Properties empColMappings) {
		this.empColMappings = empColMappings;
	}

	public Properties getSupplierColMappings() {
		return supplierColMappings;
	}

	public void setSupplierColMappings(Properties supplierColMappings) {
		this.supplierColMappings = supplierColMappings;
	}
}
