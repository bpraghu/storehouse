package pyramid.storehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pyramid.storehouse.dao.RoleDao;
import pyramid.storehouse.pojo.Role;

@Service
public class RoleService {

	@Autowired
	private RoleDao roleDao;

	public Role getRole (int roleID) {
		
		Role role = roleDao.getRole(roleID);
		return role;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
}
