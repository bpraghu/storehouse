package pyramid.storehouse.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ApplicationScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pyramid.storehouse.dao.SupplierDao;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.view.SupplierView;

@Service
@ApplicationScoped
public class SupplierService {

	@Autowired
	private SupplierDao supplierDao;

	public SupplierDao getSupplierDao() {
		return supplierDao;
	}

	public void setSupplierDao(SupplierDao supplierDao) {
		this.supplierDao = supplierDao;
	}

	public boolean saveSupplier (SupplierView supplier) {

		boolean status = true;
		SupplierView supplierFromDB = getSupplierDao ().findSupplierByMobile(supplier);

		if (supplierFromDB!=null) {
			JSFUtil.addErrorMessage ("Supplier with a mobile number " + supplier.getMobile() + " already exists!");
			status = false;
		}
		
		if (status) {
		
			status = getSupplierDao().saveSupplier(supplier);
			
			if (status) {
				JSFUtil.addMessage("New Supplier successfully created!");
			}
			else {
				JSFUtil.addErrorMessage ("New Supplier creation failed, please contact admin!");
			}
		}
		return status;
	}

	public boolean updateSupplier (SupplierView supplier) {

		boolean status = getSupplierDao().saveSupplier(supplier);

		if (status) {
			JSFUtil.addMessage("Supplier updated successfully!");
		}
		else {
			JSFUtil.addErrorMessage ("Supplier updation failed, please contact admin!");
		}
		return status;
	}

	@RequestMapping(value="/showSuppliers")
	public ModelAndView showSuppliers () {

		return new ModelAndView("supplierList");
	}

	public SupplierView getSupplier (int supplierID) {

		SupplierView supplier = supplierDao.getSupplier (String.valueOf(supplierID));
		return supplier;
	}

	public List<SupplierView> getSuppliers () {

		List<SupplierView> suppliers = supplierDao.getAllSupplier();
		return suppliers;
	}

	
	public Map<String, String> getSupplierNames () {
		List<SupplierView> suppliers = getSuppliers();
		Map<String, String> supplierNames = new HashMap<String, String>();

		for (SupplierView supplier : suppliers) {
			supplierNames.put(supplier.getSupplierName(), String.valueOf(supplier.getSupplierId()));
		}
		return supplierNames;
	}
}
