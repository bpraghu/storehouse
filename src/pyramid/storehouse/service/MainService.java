package pyramid.storehouse.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.business.MenuPermissionCommand;
import pyramid.storehouse.business.SessionHandler;
import pyramid.storehouse.dao.MenusDao;
import pyramid.storehouse.dao.RoleDao;
import pyramid.storehouse.dao.SessionmapDao;
import pyramid.storehouse.dao.StoreDao;
import pyramid.storehouse.dao.UserDao;
import pyramid.storehouse.pojo.Menu;
import pyramid.storehouse.util.CookieUtil;
import pyramid.storehouse.util.SessionUtil;
import pyramid.storehouse.view.DynamicMenus;
import pyramid.storehouse.view.StoreView;
import pyramid.storehouse.view.UserView;

@Service
public class MainService {

	private Logger logger = LoggerFactory.getLogger(MainService.class.getName());
	private StrongPasswordEncryptor spe = new StrongPasswordEncryptor();
	
	@Autowired
	SessionHandler sessionHandler;
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private SessionmapDao sessionmapDao;
	
	@Autowired
	private StoreDao storeDao;

	@Autowired
	private MenusDao menuDao;

	@Autowired
	private MenuPermissionCommand permissionCommand;

	@Autowired
	private DynamicMenus dynaMenus;

	private List<Menu> menus;
	
	public MenuPermissionCommand getPermissionCommand() {
		return permissionCommand;
	}

	public SessionmapDao getSessionmapDao() {
		return sessionmapDao;
	}

	public void setSessionmapDao(SessionmapDao sessionmapDao) {
		this.sessionmapDao = sessionmapDao;
	}

	public StoreDao getStoreDao() {
		return storeDao;
	}

	public void setStoreDao(StoreDao storeDao) {
		this.storeDao = storeDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public void setMenuDao(MenusDao menuDao) {
		this.menuDao = menuDao;
	}

	public void setPermissionCommand(MenuPermissionCommand permissionCommand) {
		this.permissionCommand = permissionCommand;
	}

	public MenusDao getMenuDao() {
		return menuDao;
	}

	public SessionHandler getSessionHandler() {
		return sessionHandler;
	}

	public void setSessionHandler(SessionHandler sessionHandler) {
		this.sessionHandler = sessionHandler;
	}

	public UserDao getUserDao() {
		return this.userDao;
	}

	public RoleDao getRoleDao() {
		return this.roleDao;
	}

	@RequestMapping(value="/authenticate", method=RequestMethod.POST)
	public boolean authenticate (UserView user) {

		logger.debug("Authenticating " + user.getUserName());

		UserDao userDao = getUserDao ();
		UserView dbUser = userDao.getUser(user);

		boolean userValidStatus = (dbUser != null && dbUser.isActive() && spe.checkPassword(user.getPassword(), dbUser.getPassword()));

		if(!userValidStatus) {
			logger.debug("User " + user.getUserName() + " is either not found or inactive or password is incorrect");
		}

		// User who has a SITE_MANAGER role and does not have a store assigned to him, is being kicked out
		if (userValidStatus	&& dbUser.getRoleId() == AppConstants.SITE_MANAGER && dbUser.getStoreId() == 0) {
			logger.debug("User " + user.getUserName() + " who has a SITE_MANAGER role and does not have a store assigned to him, is being kicked out");
			return false;
		}

		if (userValidStatus) {

			// Load menus after login
			HttpSession session = SessionUtil.getNewSession();
			List<Menu> menus = getPermissionCommand().process(dbUser);
			getSessionHandler ().createSession(dbUser);
			
			getDynaMenus().setMenus(menus);
			session.setAttribute(AppConstants.MENUS_DATA, getDynaMenus());

			//TODO from the user.getEmpId(), load the store and keep it in users the session.
			StoreView store = null;
			try {
				store = getStoreDao().getStore(dbUser.getEmpId());
			}
			catch (Exception e) {
				logger.error("An error occurred while fetching store for user " + dbUser.getEmpId() + " user name " + dbUser.getUserName());
			}
			session.setAttribute(AppConstants.MENUS_DATA, getDynaMenus());
			session.setAttribute(AppConstants.USER_DATA, dbUser);
			session.setAttribute(AppConstants.STORE, store);

			logger.debug("Authentication fine, forwarding to landing page " + dbUser.getUserName());
			return true;
		}
		else {
		//	logger.debug("Authentication failed, maybe incorrect password ");
/*			result.addError(new FieldError("user","generalError", "xxxxxxx", false, null, null, "Incorrect credentials"));
			model.addAttribute("FAILURE", "Login Failed");
			return new ModelAndView("login");
*/
			return false;
		}
	}

	public void logout () {

		sessionHandler.deactivateSession(SessionUtil.getSession().getId());
		SessionUtil.getSession().invalidate();
		CookieUtil.deleteCookie(AppConstants.SESSIONID);
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public DynamicMenus getDynaMenus() {
		return dynaMenus;
	}

	public void setDynaMenus(DynamicMenus dynaMenus) {
		this.dynaMenus = dynaMenus;
	}
	
	public static void main(String args[]) {
		StrongPasswordEncryptor spe = new StrongPasswordEncryptor();
		// System.out.println(spe.encryptPassword("gokul"));
		String dbUser = "9xyQdgUGmGayKyB3zTNf+CLCHThhp2SIbzGy4pDM39ZtwA5N8KKwttWGi38QWo2j";
		String input = "123456";
		
		System.out.println(spe.checkPassword(input, dbUser));
	}
}
