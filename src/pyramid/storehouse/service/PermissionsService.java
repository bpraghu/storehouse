package pyramid.storehouse.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pyramid.storehouse.dao.MenusDao;
import pyramid.storehouse.dao.PermissionDao;
import pyramid.storehouse.pojo.Menu;
import pyramid.storehouse.view.PermissionView;

@Service
public class PermissionsService {

	private static final Logger logger = LoggerFactory.getLogger(PermissionsService.class);

	@Autowired
	private MenusDao menusDao;

	@Autowired
	private PermissionDao permissionDao;

	public List<Menu> getAllMenusForRole (int roleId) {

		List<Menu> menus = null;
		try {
			menus = menusDao.getAllMenusForRole(roleId);
		}
		catch (Exception e) {
			logger.error("An exception occurred while trying getting menus for role " + roleId, e);
		}
		return menus;
	}

	public boolean updatePermission (PermissionView permission) {

		boolean status = false;
		try {
			permissionDao.updatePermission (permission);
			status = true;
		}
		catch (Exception e) {
			logger.error("An exception occurred while trying to update permissions for role " + permission.getRoleId() + " and menuId " + permission.getMenuId(), e);
			status = false;
		}
		return status;
	}

	public boolean updatePermission (int roleId, Menu menu) {

		boolean status = false;
		try {
			permissionDao.updatePermission (roleId, menu);
			status = true;
		}
		catch (Exception e) {
			logger.error("An exception occurred while trying to update permissions for role " + roleId + " and menuId " + menu.getMenuId(), e);
			status = false;
		}
		return status;
	}
}
