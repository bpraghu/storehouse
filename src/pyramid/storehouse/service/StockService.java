package pyramid.storehouse.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.dao.ItemDao;
import pyramid.storehouse.dao.StockDao;
import pyramid.storehouse.dao.StoreDao;
import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.util.SessionUtil;
import pyramid.storehouse.view.ItemView;
import pyramid.storehouse.view.SearchView;
import pyramid.storehouse.view.StockView;
import pyramid.storehouse.view.StoreView;
import pyramid.storehouse.view.UserView;

@Service
@Transactional
public class StockService {
	
	private Logger logger =LoggerFactory.getLogger(StockService.class);

	@Autowired
	private ItemDao itemDao;

	@Autowired
	private StockDao stockDao;

	@Autowired
	private StoreDao storeDao; 

	public StockView getStockUpdate (StockView stock) {

		StoreView store = (StoreView)SessionUtil.getSessionData(AppConstants.STORE);
		StockView stockUpdate = stockDao.getStockUpdateByStoreAndStockID (store.getStoreId(), stock.getItemId());
		return stockUpdate;
	}
	
	public Map<Integer, StockView> getStoreStockUpdate () {

		StoreView store = (StoreView)SessionUtil.getSessionData(AppConstants.STORE);
		List<StockView> stockUpdateList = stockDao.getAllStockUpdates(store.getStoreId());
		Map<Integer, StockView> stockMap = new HashMap<Integer, StockView>();

		for (StockView stockView : stockUpdateList) {
			stockMap.put(stockView.getItemId(), stockView);
		}
		return stockMap;
	}


	@Transactional(readOnly = false, propagation=Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean saveStock (StockView stock) {

		boolean stockEntryStatus = false;
		StoreView store = (StoreView)SessionUtil.getSessionData(AppConstants.STORE);

		stock.setEntryDate( DateUtil.getCurrentDate());
		stock.setOperation (AppConstants.INCOMING_STOCK);
		stock.setStoreId(store.getStoreId());

		UserView user = (UserView)SessionUtil.getSessionData(AppConstants.USER_DATA);
		stock.setEmployeeId(String.valueOf(user.getEmpId()));

		try {
			stockDao.stockEntry(stock);
			stockDao.stockBalanceUpdate(stock);
			stockEntryStatus = true;
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("An exception happened while dispersing "+ stock.toString(), e);
			stockEntryStatus = false;
			throw e;
		}
		return stockEntryStatus;
	}

	public boolean saveEditedStock (StockView stock) {

		boolean stockUpdateStatus = false;

		UserView user = (UserView)SessionUtil.getSessionData(AppConstants.USER_DATA);
		stock.setEmployeeId(String.valueOf(user.getEmpId()));

		StockView originalTran = getTransaction(stock);		
		// Check if the item id has changed
		if(originalTran.getItemId() == stock.getItemId()) {
			// Since itemid hasnt changed, just go ahead and update the rest of the data in the StockTable
			try {
				// Update StockBalance here.
				Long updatedQuantityBalance = stock.getQuantity() - originalTran.getQuantity();
				Long updatedVolumeBalance = null;

				if (stock.getVolume() != null && originalTran.getVolume() != null) {
					updatedVolumeBalance = stock.getVolume() - originalTran.getVolume();
				}
				else if (stock.getVolume() == null && originalTran.getVolume() != null) {
					updatedVolumeBalance = originalTran.getVolume();
				}
				else if (stock.getVolume() != null && originalTran.getVolume() == null) {
					updatedVolumeBalance = stock.getVolume();
				}

				originalTran.setQuantity (updatedQuantityBalance);
				originalTran.setVolume (updatedVolumeBalance);

				stockDao.stockBalanceUpdate(originalTran);
				
				originalTran.setModifyDate(DateUtil.getCurrentDate());
				originalTran.setEmployeeId(stock.getEmployeeId());
				originalTran.setQuantity(stock.getQuantity());
				originalTran.setVolume(stock.getVolume());
				originalTran.setSupplierId(stock.getSupplierId());
				originalTran.setDriverMobile(stock.getDriverMobile());
				originalTran.setVehicleRegNum(stock.getVehicleRegNum());
				originalTran.setRemarks(stock.getRemarks());

				stockDao.stockUpdate(originalTran);
				stockUpdateStatus = true;

			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("An exception happened while editing "+ stock.toString(), e);
				JSFUtil.addErrorMessage("An error happened while saving the edited transaction. Please contact the admin ..!");
				stockUpdateStatus = false;
				throw e;
			}
		}
		else {
			try {
				// If the itemID has changed, substract the old quantity and volume from the previous store and item id entry from StockBalance table 
				Long updatedQuantityBalance = originalTran.getQuantity() * -1;
				Long updatedVolumeBalance = originalTran.getVolume() * -1;

				originalTran.setQuantity (updatedQuantityBalance);
				originalTran.setVolume (updatedVolumeBalance);
				stockDao.stockBalanceUpdate(originalTran);

				// Now update the new ItemID and the update stock settings and then update
				originalTran.setItemId(stock.getItemId());
				originalTran.setQuantity(stock.getQuantity());
				originalTran.setVolume(stock.getVolume());
				originalTran.setModifyDate(DateUtil.getCurrentDate());
				originalTran.setEmployeeId(stock.getEmployeeId());
				originalTran.setSupplierId(stock.getSupplierId());
				originalTran.setDriverMobile(stock.getDriverMobile());
				originalTran.setVehicleRegNum(stock.getVehicleRegNum());
				originalTran.setRemarks(stock.getRemarks());

				stockDao.stockUpdate(originalTran);
				stockDao.stockBalanceUpdate(originalTran);
				stockUpdateStatus = true;
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("An exception happened while editing "+ stock.toString(), e);
				JSFUtil.addErrorMessage("An error happened while saving the edited transaction. Please contact the admin ..!");
				stockUpdateStatus = false;
				throw e;
			}
		}
		
		return stockUpdateStatus;
	}
	
	@Transactional(readOnly = false, propagation=Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean saveStockDisperse (StockView stock) {

		boolean stockEntryStatus = false;
		long originalStock = stock.getQuantity();
		long originalVolume = 0;
		if(stock.getVolume()!=null) {
			originalVolume = stock.getVolume();
		}
		StoreView store = (StoreView)SessionUtil.getSessionData(AppConstants.STORE);

		stock.setEntryDate(Calendar.getInstance().getTime());
		stock.setOperation (AppConstants.OUTGOING_STOCK);
		stock.setStoreId(store.getStoreId());
		stock.setQuantity(-1 * stock.getQuantity());
		
		if(stock.getVolume() != null) {
			stock.setVolume(-1 * stock.getVolume());
		}

		stock.setSupplierId(null);

		UserView user = (UserView)SessionUtil.getSessionData(AppConstants.USER_DATA);
		stock.setEmployeeId(String.valueOf(user.getEmpId()));

		try {
			stockDao.stockEntry(stock);
			stockDao.stockBalanceUpdate(stock);
			stockEntryStatus = true;
		}
		catch (Exception e) {
			stockEntryStatus = false;
			e.printStackTrace();
			stock.setQuantity(originalStock);
			stock.setVolume(originalVolume);
			throw e;
		}

		return stockEntryStatus;
	}

	
	@Transactional(readOnly = false, propagation=Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean saveEditStockDisperse (StockView stock) {

		boolean stockUpdateStatus = false;
	//	long originalStock = stock.getQuantity();
	/*	long originalVolume = 0;
		if(stock.getVolume()!=null) {
			originalVolume = stock.getVolume();
		}*/

		UserView user = (UserView)SessionUtil.getSessionData(AppConstants.USER_DATA);
		stock.setEmployeeId(String.valueOf(user.getEmpId()));

		StockView originalTran = getTransaction(stock);		
		// Check if the item id has changed
		if(originalTran.getItemId() == stock.getItemId()) {
			// Since itemid hasnt changed, just go ahead and update the rest of the data in the StockTable
			try {
				// Update StockBalance here.
				Long updatedQuantityBalance = originalTran.getQuantity() - stock.getQuantity();
				Long updatedVolumeBalance = null;

				if (stock.getVolume() != null && originalTran.getVolume() != null) {
					updatedVolumeBalance = originalTran.getVolume() - stock.getVolume();
				}
				else if (stock.getVolume() == null && originalTran.getVolume() != null) {
					updatedVolumeBalance = (-1 * originalTran.getVolume());
				}
				else if (stock.getVolume() != null && originalTran.getVolume() == null) {
					updatedVolumeBalance = (-1 * stock.getVolume());
				}

				originalTran.setQuantity (updatedQuantityBalance);
				originalTran.setVolume (updatedVolumeBalance);

				stockDao.stockBalanceUpdate(originalTran);
				
				originalTran.setModifyDate(DateUtil.getCurrentDate());
				originalTran.setEmployeeId(stock.getEmployeeId());
				originalTran.setQuantity(stock.getQuantity());
				originalTran.setVolume(stock.getVolume());
				originalTran.setSupplierId(stock.getSupplierId());
				originalTran.setDriverMobile(stock.getDriverMobile());
				originalTran.setVehicleRegNum(stock.getVehicleRegNum());
				originalTran.setRemarks(stock.getRemarks());

				stockDao.stockUpdate(originalTran);
				stockUpdateStatus = true;
			}
			catch (Exception e)  {
				e.printStackTrace();
				logger.error("An exception happened while editing incoming stock "+ stock.toString(), e);
				JSFUtil.addErrorMessage("An error happened while saving the edited incoming transaction. Please contact the admin ..!");
				stockUpdateStatus = false;
				throw e;
			}
		}
		else {
			
			try {
				
				StockView stockUpdate = getStockUpdate(stock);
				
				if (stockUpdate == null) {
					ItemView item = itemDao.getItem(stock.getItemId());
					JSFUtil.showErrorMessage("Not enough stock, Cannot disperse " + item.getName() + ", current balance is zero!");
					return false;
				}
				else if (stock.getQuantity() > stockUpdate.getQuantity()) {
					ItemView item = itemDao.getItem(stock.getItemId());
					JSFUtil.showErrorMessage("Not enough stock, " + item.getName() + " has only " + stockUpdate.getQuantity() + " in stock.");
					return false;
				}
				else if (stock.getVolume() != null && (stock.getVolume() > stockUpdate.getVolume())) {
					ItemView item = itemDao.getItem(stock.getItemId());
					JSFUtil.showErrorMessage("Not enough stock, " + item.getName() + " has only " + stockUpdate.getVolume() + " in stock.");
					return false;
				}
				
				
				// If the itemID has changed, substract the old quantity and volume from the previous store and item id entry from StockBalance table 
				/*
				Long updatedQuantityBalance = originalTran.getQuantity();
				Long updatedVolumeBalance = originalTran.getVolume();

				originalTran.setQuantity (updatedQuantityBalance);
				originalTran.setVolume (updatedVolumeBalance);
				*/

				// This call is to add back the deducted numbers from the previous disperse transaction
				stockDao.stockBalanceUpdate(originalTran);

				// Now update the new ItemID and the update stock settings and then update
				originalTran.setItemId(stock.getItemId());
				originalTran.setQuantity((-1 * stock.getQuantity()));
				
				if(stock.getVolume() != null) {
					originalTran.setVolume((-1 * stock.getVolume()));
				}
				originalTran.setModifyDate(DateUtil.getCurrentDate());
				originalTran.setEmployeeId(stock.getEmployeeId());
				originalTran.setSupplierId(stock.getSupplierId());
				originalTran.setDriverMobile(stock.getDriverMobile());
				originalTran.setVehicleRegNum(stock.getVehicleRegNum());
				originalTran.setRemarks(stock.getRemarks());

				stockDao.stockUpdate(originalTran);
				stockDao.stockBalanceUpdate(originalTran);
				stockUpdateStatus = true;
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("An exception happened while editing disperesed " + stock.toString(), e);
				JSFUtil.addErrorMessage("An error happened while saving the edited dispersed transaction. Please contact the admin ..!");
				stockUpdateStatus = false;
				throw e;
			}
		}

		/*stock.setEntryDate(Calendar.getInstance().getTime());
		stock.setOperation (AppConstants.OUTGOING_STOCK);
		stock.setStoreId(store.getStoreId());
		stock.setQuantity(-1 * stock.getQuantity());
		
		if(stock.getVolume() != null) {
			stock.setVolume(-1 * stock.getVolume());
		}

		stock.setSupplierId(null);

		try {
			stockDao.stockEntry(stock);
			stockDao.stockBalanceUpdate(stock);
			stockEntryStatus = true;
		}
		catch (Exception e) {
			stockEntryStatus = false;
			e.printStackTrace();
			stock.setQuantity(originalStock);
			stock.setVolume(originalVolume);
			throw e;
		}*/

		return stockUpdateStatus;
	}

	public List<StockView> getTransactions (SearchView query) {

		List<StockView> transList = null;

		try {
			Date fromDate = DateUtil.setBODTimeStamp(query.getEntryDate());
			Date tillDate = DateUtil.setEODTimeStamp(query.getEntryDate());
			transList = stockDao.getStockTransactions(query.getStoreId(), query.getItemId(), fromDate, tillDate, query.getOperation());
			setStoreNames (transList);
		}
		catch (Exception e) {
			logger.error("An error occurred while retreiving all stock transactions ", e);
		}
		return transList;
	}

	private void setStoreNames (List<StockView> transList) {

		List<StoreView> stores = storeDao.getAllStores();
		HashMap<Integer, String> storeMap = new HashMap<Integer, String> ();
		for(StoreView st : stores) {
			storeMap.put(st.getStoreId(), st.getStoreName());
		}
		
		for (StockView sv : transList) {
			sv.setToStoreName(storeMap.get(sv.getToStoreId()));
			sv.setFromStoreName(storeMap.get(sv.getFromStoreId()));
		}
	}
	
	public StockView getTransaction (StockView query) {

		StockView tran = null;

		try {
			tran = stockDao.getStockTransaction(query.getStockId());
		}
		catch (Exception e) {
			logger.error("An error occurred while retreiving all stock transactions ", e);
		}
		return tran;
	}

	public StoreDao getStoreDao() {
		return storeDao;
	}

	public void setStoreDao(StoreDao storeDao) {
		this.storeDao = storeDao;
	}
}
