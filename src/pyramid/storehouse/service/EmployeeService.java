package pyramid.storehouse.service;

import java.util.HashMap;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pyramid.storehouse.AppConstants;
import pyramid.storehouse.dao.EmployeeDao;
import pyramid.storehouse.dao.StoreDao;
import pyramid.storehouse.dao.UserDao;
import pyramid.storehouse.pojo.Role;
import pyramid.storehouse.util.JSFUtil;
import pyramid.storehouse.view.EmployeeView;
import pyramid.storehouse.view.StoreView;
import pyramid.storehouse.view.UserView;

@Service
public class EmployeeService {

	private StrongPasswordEncryptor spe = new StrongPasswordEncryptor();
	private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);
	
	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private StoreDao storeDao;

	public boolean employeeSave(EmployeeView emp) {

		boolean saveStatus = false;
		EmployeeView searchedEmp = employeeDao.findEmployeeByMobile(emp);
		if (searchedEmp != null) {
			JSFUtil.addErrorMessage("An employee with Mobile number " + searchedEmp.getMobile() + " already exists");
			return saveStatus;
		}

		try {
			employeeDao.saveEmployee(emp);
			saveStatus = true;
		}
		catch (Exception e) {
			saveStatus = false;
			logger.error("An Error occurred while trying to save employee " + emp.toString(), e);
		}

		if (saveStatus) {
			emp.reset();
			JSFUtil.addMessage("New Employee successfully created!");
		} else {
			JSFUtil.addErrorMessage("New Employee creation failed, please contact admin!");
		}
		return saveStatus;
	}

	public boolean employeeEditSave(EmployeeView emp) {

		boolean saveStatus = false;

		try {
			employeeDao.saveEmployee(emp);
			saveStatus = true;
		}
		catch (Exception e) {
			saveStatus = false;
			logger.error("An Error occurred while trying to save employee " + emp.toString(), e);
		}
		return saveStatus;
	}

	@Transactional(readOnly = false, propagation=Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean updateEmployeeStatus(EmployeeView emp) {

		boolean saveStatus = false;
		try {
			employeeDao.saveEmployee(emp);
			StoreView store = null;
			
			try {
				store = storeDao.getStore(emp.getEmpId());
			}
			catch (Exception e) {
				logger.debug("Employee ID " + emp.getEmpId() + " not linked to any store");
			}

			if(store != null && AppConstants.INACTIVE.equals(emp.getActive())) {
				store.setEmpId(0);
				storeDao.saveStore(store);
			}
			saveStatus = true;
		}
		catch (Exception e) {
			saveStatus = false;
			logger.error("An Error occurred while trying to save employee " + emp.toString(), e);
			throw e;
		}
		return saveStatus;
	}

	private HashMap<String, String> getRoleNameMap(List<Role> roles) {

		HashMap<String, String> roleMap = new HashMap<String, String>();

		for (Role role : roles) {
			roleMap.put(role.getRoleId().toString(), role.getRoleName());
		}
		return roleMap;
	}

	public EmployeeView getEmployee(int empID) {

		return employeeDao.getEmployee(empID);
	}

	public List<EmployeeView> getEmployees() {

		return employeeDao.getAllEmployees();
	}

	public List<EmployeeView> getUnlinkedEmployees () {

		return employeeDao.getUnlinkedEmployees();
	}

	public boolean createLogin (UserView user) {

		boolean status = true;
		this.changePassword(user);
		status = userDao.createLogin(user);
		return status;
	}

	public boolean changePassword (UserView user) {

		boolean status = true;
		user.setPassword(spe.encryptPassword(user.getPassword()));
		status = userDao.changePassword(user);
		return status;
	}

	public boolean isUserNameAlreadyExisting (UserView user) {

		boolean userAlreadyExists = false;
		try {
			// IF an exception is throws, assuming user already exists and not allowing userid creation 
			userDao.getUser(user.getUserName().trim());
			userAlreadyExists = true;
		}
		catch (Exception e) {
			logger.error ("An exception occurred while checking if user exists, username " + user.getUserName() , e);
			userAlreadyExists = false;
		}
		return userAlreadyExists;
	}

	public UserView getUser (UserView user) {

		return userDao.getUser(user);
	}
}
