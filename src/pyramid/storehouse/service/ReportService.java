package pyramid.storehouse.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pyramid.storehouse.dao.ReportDao;
import pyramid.storehouse.util.DateUtil;
import pyramid.storehouse.view.ReportDataView;
import pyramid.storehouse.view.ReportView;
import pyramid.storehouse.view.TransferView;

@Service
public class ReportService {

	@Autowired
	private ReportDao reportDao;

	public List<ReportDataView> generateItemWiseReport (ReportView report) {

		List<ReportDataView> reports = reportDao.generateStoreWiseItemReport(report);
		return reports;
	}

	public List<ReportDataView> generateStoreWiseItemReport (ReportView report) {

		List<ReportDataView> reports = reportDao.getStoreWiseItemReport (report.getStoreId(), report.getEndDate());
		return reports;
	}

	public List<ReportDataView> generateStockUpdateForStoreReport (ReportView report) {

		List<ReportDataView> reports = reportDao.getStockUpdateForStoreTillDate(report.getStoreId(), report.getEndDate());
		return reports;
	}

	public List<TransferView> generateTransferReport (TransferView transferView) {

		Date fromDate = DateUtil.setBODTimeStamp(transferView.getFromDate());
		Date endDate = DateUtil.setEODTimeStamp(transferView.getEndDate());
		List<TransferView> transfers = reportDao.getTransferFromReport(transferView.getStoreId(), fromDate, endDate);
		return transfers;
	}
	
	public List<TransferView> generateTransferToReport (TransferView transferView) {

		Date fromDate = DateUtil.setBODTimeStamp(transferView.getFromDate());
		Date endDate = DateUtil.setEODTimeStamp(transferView.getEndDate());
		List<TransferView> transfers = reportDao.getTransferToReport(transferView.getStoreId(), fromDate, endDate);
		return transfers;
	}

	public ReportDao getReportDao() {
		return reportDao;
	}

	public void setReportDao(ReportDao reportDao) {
		this.reportDao = reportDao;
	}
}
