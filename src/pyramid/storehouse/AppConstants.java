package pyramid.storehouse;

public interface AppConstants {

	// Constants updated
	public static final String APPLICATION_DATA = "APP_DATA";
	public static final String APPLICATION_CONFIG_DATA = "APP_CONFIG_DATA";
	public static final String SESSION_DATA = "SESSION_DATA";
	public static final String CLIENT_KEY = "ck";
	public static final String MENUS_DATA = "MENUS";
	public static final String STORE = "STORE";
	public static final String USER_DATA = "USER_DATA";
	public static final String REDIRECT = "redirect";
	public static final String CRUD = "CRUD";
	public static final String INCOMING_STOCK = "I";
	public static final String OUTGOING_STOCK = "O";
	public static final String OUTGOING_STOCK_TRANSFER = "OT";
	public static final String INCOMING_STOCK_TRANSFER = "IT";
	public static final String NEW_STOCK = "NEW STOCK";
	public static final String DISPERSED_STOCK = "DISPERSED STOCK";

	public static final String EMPID_FOR_PASSWORD_CHANGE = "EMPID_FOR_PASSWORD_CHANGE";
	public static final String SESSIONID = "JSESSIONID";
	public static final String ACTIVE = "A";
	public static final String INACTIVE = "I";
	public static final int    ADMIN = 1;
	public static final int    SITE_MANAGER = 2;
	public static final int    BOSS = 3;
	public static final String NEW = "NEW";
	public static final String EDIT = "EDIT";
}
