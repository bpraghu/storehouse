package pyramid.storehouse;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pyramid.storehouse.business.SessionHandler;
import pyramid.storehouse.dao.AppConfigDao;
import pyramid.storehouse.dao.SessionmapDao;
import pyramid.storehouse.util.SessionUtil;
import pyramid.storehouse.util.StringUtils;
import pyramid.storehouse.view.DynamicMenus;

public class JSFAuthenticationFilter implements Filter {

	private static Logger logger = LoggerFactory.getLogger(JSFAuthenticationFilter.class);
	
	@Autowired
	private SessionmapDao sessionmapDao;
	
	@Autowired
	SessionHandler sessionHandler;
	
	@Autowired
	AppConfigDao appConfigDao;

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest req1, ServletResponse resp1, FilterChain fc)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)req1;
		HttpServletResponse resp = (HttpServletResponse)resp1;
		HttpSession session = req.getSession(false);
		String contextPath = req.getContextPath();

		ServletContext sc = req.getServletContext();

		if( sc.getAttribute(AppConstants.APPLICATION_CONFIG_DATA) == null) {
			AppConfigMap configs = appConfigDao.getAllConfigs();
			sc.setAttribute(AppConstants.APPLICATION_CONFIG_DATA, configs);
		}

		String methodName = req.getMethod();
		String crudKey = req.getParameter(AppConstants.CRUD);
		// Do not check for authentication for login and authenticate requests
		if(req.getRequestURI().contains("/authenticate.xhtml")) {
			return ;
		}

		String sessionID = null;

		if (session != null) {
			sessionID =	session.getId();
		}

		if(!req.getRequestURI().contains("/jsf/") && req.getRequestURI().contains("javax.faces.resource")) {

			fc.doFilter(req, resp);
			return;
		}

		logger.debug("Remote IP " + req.getRemoteAddr());
		//Logging only requests that will contain the post requests
		logger.debug(getRequestData(req));

		// If clientKey not found then log the user out and also do not allow any GET calls to the app, just log them off
		//if(req.getMethod().equals("GET") || clientKey == null) {
		if(req.getRequestURI().contains("/login.xhtml") || req.getRequestURI().contains("/logout.xhtml")) {

			fc.doFilter(req, resp);
			return;
		}

		sessionHandler.updateLastAccessed(sessionID);
		
		if (session == null || session.getAttribute(AppConstants.USER_DATA) == null) {

			resp.sendRedirect(contextPath + "/jsf/login.xhtml");
			return ;
		}
		
		resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		resp.setDateHeader("Expires", 0); // Proxies.

		if ("POST".equalsIgnoreCase(methodName) && StringUtils.isNotEmpty(crudKey)) {
			if ((!req.getRequestURI().contains("report")) && session.getAttribute(crudKey)!=null) {
				session.removeAttribute(AppConstants.CRUD);
				resp.sendRedirect (contextPath + "/templateTest.xhtml?" + AppConstants.CLIENT_KEY + "=" + sessionID);
				return ;
			}
			else {
				session.setAttribute(AppConstants.CRUD, crudKey);
			}
		}

		DynamicMenus menus = (DynamicMenus)session.getAttribute(AppConstants.MENUS_DATA);
		req.setAttribute(AppConstants.MENUS_DATA, menus);
		req.setAttribute(AppConstants.STORE, session.getAttribute(AppConstants.STORE));

		fc.doFilter(req, resp);
	}

	private String getRequestData (HttpServletRequest request) {

		StringBuffer data = new StringBuffer();
		Map<String, String[]> dataMap = request.getParameterMap();
		
		//String sessionID = SessionUtil.getSession()!=null?SessionUtil.getSession().getId():"1111111111111";
	//	data.append("SessionID = " + sessionID + " ");

		for (Entry<String, String[]> param : dataMap.entrySet()) {

			data.append(param.getKey());
			data.append("=");
			if (param.getKey().contains("password")) {
				data.append("**************");
			}
			else {
				data.append(getArrayValue(param.getValue()));
			}
			data.append("&");
		}
		
		if (data.length() > 0)
			data.deleteCharAt(data.length() - 1);

		return data.toString();
	}
/*
	private boolean isAjaxCall (HttpServletRequest req) {

		return req.getParameterMap().containsKey("javax.faces.partial.ajax");
	}
*/
	private String getArrayValue (String array[]) {
		
		StringBuffer sb = new StringBuffer ();
		
		for (String data : array) {
			sb.append(data);
			sb.append("|");
		}
		if(sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	public void init(FilterConfig cfg) throws ServletException {

		ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(cfg.getServletContext());
		this.sessionmapDao = ctx.getBean(SessionmapDao.class);
		this.sessionHandler = ctx.getBean(SessionHandler.class); 
		this.appConfigDao = ctx.getBean(AppConfigDao.class);
	}
}
