function submitForm (action) {
	var form = document.getElementById("mainForm");
	form.method = 'POST';
	form.action = contextPath + '/' + action + '.xhtml';
	form.submit();
}

function submitSupplierSearchForm (action, view, method) {
	var form = $("#mainForm");

	if(method) {
		form.attr ('METHOD', method);
	}

	form.append('<input type="hidden" name="viewName" value="' + view + '"/>');
	form.attr ('action', '/storehouse/action/' + action + '.htm');
	form.submit();
}

function validateForm (formName) {
	var input = $( "form input:text" );
	var validated = true;
	for (var i = 0; i < input.length; i++) {
		$(input[i].parentNode).append('<span class="display-error" >' + input[i].name + ' is a mandatory field</span>');
		validated = false;
	}
	return validated;
}

function openpopup (_url, _name, _width, _height, _status, _resizable) {
    var left = (screen.width/2) - (_width/2);
    var top = (screen.height/2) - (_height/2);

    popupwindow = window.open(_url,_name,"top=" + top +",left=" + left + ",height=" + _height + ",width=" + _width + ",status=" + _status + ",resizable=" + _resizable + ",toolbar=no,scrollbar=no,menubar=no");
    if (popupwindow !== null) {
        popupwindow.focus();
    }
    return popupwindow;
};